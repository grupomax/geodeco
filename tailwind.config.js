const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.js',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            gridTemplateRows: {
                '14': 'repeat(14, minmax(0, 1fr))'
            },
        },
        zIndex: {
            '0': 0,
            '10': 10,
            '20': 20,
            '30': 30,
            '40': 40,
            '50': 50,
            '25': 25,
            '50': 50,
            '75': 75,
            '100': 100,
            'auto': 'auto',
        },
    },

    variants: {
        extend: {
            opacity: ['disabled'],
            zIndex: ['hover'],
            scrollbar: ['rounded']
        },
    },

    plugins: [
        require('@tailwindcss/forms'),
        require('tailwind-scrollbar')
    ],
};
