<?php

namespace Database\Factories;

use App\Models\Categoria;
use App\Models\Producto;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Producto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->words(3,true),
            'slug' => $this->faker->slug(),
            'current_img' => $this->faker->imageUrl(),
            'category_id' => $this->faker->numberBetween(1,Categoria::count()),
            'description' => $this->faker->text(140),
            'price' => $this->faker->randomFloat(2,1000,300000),
            'width' => $this->faker->randomFloat(2,1,30),
            'height' => $this->faker->randomFloat(2,1,30),
            'depth' => $this->faker->randomFloat(2,1,30),
            'offer' => $this->faker->boolean(20),
            'discount' => $this->faker->numberBetween(0,100),
            'on_stock' => $this->faker->boolean(70)
        ];
    }
}
