<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(1)->create([
            'name' => 'Geo Pioletti',
            'email' => 'piolettigeovanafernanda@gmail.com',
            'password' => bcrypt('glitterGeo2021')
        ]);
        User::factory(1)->create([
            'name' => 'Lucas Guzman',
            'email' => 'lucasgasguzman@gmail.com',
            'password' => bcrypt('paciencia')
        ]);
        User::factory(1)->create([
            'name' => 'Lucio Palacio',
            'email' => 'luciopalacio95@gmail.com',
            'password' => bcrypt('lucio123')
        ]);

        //$this->call(CategoriaSeeder::class);
        //$this->call(ProductoSeeder::class);
    }
}
