<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\PrincipalController;
use App\Http\Controllers\CategoriasController;
use App\Http\Controllers\ProductosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PrincipalController::class, 'index']);

Route::get('/categoria/{categoria}', [CategoriasController::class, 'show'])->name('categoria');
Route::post('/categoria/api/send/email', [PrincipalController::class, 'mail']);

Route::get('/producto/{producto}', [ProductosController::class, 'show'])->name('producto');
Route::post('/producto/api/send/email', [PrincipalController::class, 'mail']);


Route::prefix('categorias')->middleware(['auth', 'verified'])->group(function () {
    Route::get('/', [CategoriasController::class, 'lists'])->name('categorias');
    Route::get('/create',[CategoriasController::class, 'create'])->name('categorias.create');
    Route::post('/toggle-category', [CategoriasController::class, 'toggleCategory']);
    Route::get('/editar/{id}', [CategoriasController::class, 'edit'])->name('categorias.edit');
    Route::post('/canslug', [CategoriasController::class, 'canslug']);
    Route::post('/update', [CategoriasController::class, 'update']);
    Route::post('/store', [CategoriasController::class, 'store']);
    Route::delete('/destroy/{id}', [CategoriasController::class, 'destroy']);
});

Route::prefix('productos')->middleware(['auth','verified'])->group(function () {
    Route::get('/',[ProductosController::class, 'index'])->name('productos');
    Route::get('/create',[ProductosController::class, 'create'])->name('productos.create');
    Route::post('/toggle-product', [ProductosController::class, 'toggleProduct']);
    Route::post('/destroy', [ProductosController::class, 'destroy']);
    Route::get('/editar/{id}', [ProductosController::class, 'edit'])->name('productos.edit');
    Route::post('/canslug', [ProductosController::class, 'canslug']);
    Route::post('/canslugoncreate', [ProductosController::class, 'canslugoncreate']);
    Route::post('/update', [ProductosController::class, 'update']);
    Route::post('/store', [ProductosController::class, 'store']);
    Route::post('/update/current-img', [ProductosController::class, 'updateCurrentImg']);
    Route::post('/img/delete',[ProductosController::class,'deleteImg']);
    Route::post('/uploadImg',[ProductosController::class,'uploadImg']);
});

require __DIR__.'/auth.php';

