<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Email extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($consulta)
    {
        $this->consulta = $consulta;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Consulta en Geodeco')
                    ->from('noreply@geodeco.com.ar', 'Geodeco')
                    ->replyTo($this->consulta["email"])
                    ->view('email.consulta')
                    ->with([
                            'nombre' => $this->consulta["nombre_apellido"],
                            'email' => $this->consulta["email"],
                            'telefono' => $this->consulta["telefono"],
                            'from' => $this->consulta["from"],
                            'to' => $this->consulta["to"],
                            'comentario' => nl2br($this->consulta["comentario"])
                        ]);
    }
}
