<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Categoria;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'products';
    protected $fillable = [
        'name',
        'slug',
        'current_img',
        'category_id',
        'description',
        'price',
        'width',
        'height',
        'depth',
        'offer',
        'discount',
        'on_stock'
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function categorias(){
        return $this->hasOne(Categoria::class,'id','category_id')->withTrashed();
    }
}
