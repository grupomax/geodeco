<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Storage;
use App\Models\Categoria;
use App\Models\Producto;
use Illuminate\Http\Request;
use Inertia\Inertia;


class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $disk = Storage::disk('public');
        $productos = Producto::with('categorias')->latest()->get();
        foreach ($productos as $producto){
            $aux_image = $disk->allFiles("productos/$producto->id");
            $array_urls = [];
            foreach($aux_image as $image){
                array_push($array_urls, $disk->url($image));
            }
            $producto->url_imgs = $array_urls;
        }
        $categorias = Categoria::count();
        return Inertia::render('Productos/Lists',[
            'productos' => $productos,
            'categorias' => $categorias
        ]);
    }

    public function toggleProduct(Request $request){
        $producto = Producto::withTrashed()->findOrFail($request->product_id);
        if(!$producto->on_stock){
            $disk = Storage::disk('public');
            if(!$disk->allfiles("productos/$producto->id")){
                return response()->json(['status' => false, 'msg' => 'No se puede activar un producto que no tiene ninguna imagen'],200);
            }
        }
        $producto->on_stock = !$producto->on_stock;
        $producto->save();
        return response()->json(['status'=> true],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::select(['id','name'])->get();
        return Inertia::render('Productos/Create',[
            'categorias' => $categorias
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = Producto::create($request->all());
        $producto->on_stock = false;
        $producto->save();
        $disk = Storage::disk('public');
        $aux_img = $disk->allFiles("productos/$producto->id");
        $array_urls = [];
        foreach($aux_img as $aux){
            array_push($array_urls, $disk->url($aux));
        }
        $producto->url_imgs = $array_urls;
        return response()->json(['status' => true, 'prod'=> $producto]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($product_id)
    {
        $disk = Storage::disk('public');
        $product = Producto::select(['*'])->where('slug',$product_id)->first();
        $categorias = Categoria::select(['id','name','slug'])->get();
        if($product){
            $productos = Producto::select('*')->where('id', $product->id)->get();
            $recomendados = Producto::select(
                ['products.name as producto_name','products.slug as producto_slug','products.current_img as current_img',
                 'products.id as producto_id', 'categories.id as categoria_id',
                 'categories.name as categoria_name', 'categories.slug as categoria_slug',
                 'products.offer as producto_oferta'
            ])->join('categories', 'products.category_id', "=", 'categories.id')
            ->where('products.on_stock', true)
            ->where('category_id', $product->category_id)
            ->where('products.id', '!=', $product->id)
            ->latest('products.created_at')
            ->get();

            foreach ($recomendados as $recomendado){
                if(!$recomendado->current_img){
                    $aux_image_exist_r = $disk->allFiles("productos/$recomendado->producto_id");
                    if($aux_image_exist_r){
                        $aux_image_r = $disk->allFiles("productos/$recomendado->producto_id")[0];
                        $recomendado->url_img = $disk->url($aux_image_r);
                    }else{
                        $recomendado->url_img = '/images/Imagen_no_disponible.jpg';
                    }
                }
                else{
                    $recomendado->url_img = $recomendado->current_img;
                }
            }

            $imagenes = [];
            $aux_images = $disk->allFiles("productos/$product->id");

            if($aux_images){
                foreach($aux_images as $aux_image){
                    array_push($imagenes, $disk->url($aux_image));
                }
            }else{
                array_push($imagenes, '/images/Imagen_no_disponible.jpg');
            }


            return Inertia::render('Producto',[
                'producto' => $productos,
                'imagenes' => $imagenes,
                'categorias' => $categorias,
                'recomendados' => $recomendados
            ]);
        }
        return Inertia::render('Error',[
            'categorias' => $categorias
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::withTrashed()->where('id',$id)->first();
        if($producto){
            $disk = Storage::disk('public');
            $aux_image = $disk->allFiles("productos/$producto->id");
            $array_urls = [];
            foreach($aux_image as $image){
                array_push($array_urls, $disk->url($image));
            }
            $producto->url_imgs = $array_urls;

            $categorias = Categoria::withTrashed()->get();
            return Inertia::render('Productos/Edit', [
                'producto' => $producto,
                'categorias' => $categorias,
            ]);
        } else {
            return Inertia::render('Error');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $producto = Producto::findOrFail($request->id);
        $producto->update($request->all());
        return response()->json(['status' => true, 'prod'=> $producto]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $producto = Producto::findOrFail($request->producto_id);
        $producto->delete();
        $disk = Storage::disk('public');
        $archivos = $disk->allFiles("productos/$request->producto_id");
        foreach($archivos as $archivo){
            $disk->delete($archivo);
        }
        return response()->json(['status' => true]);
    }

    public function canslug(Request $request){
        $producto = Producto::where('slug',$request->slug)->where('id','!=',$request->prod_id)->first();
        if($producto){
            return response()->json(['status'=> false],200);
        }
        return response()->json(['status'=> true],200);
    }

    public function canslugoncreate(Request $request){
        $producto = Producto::where('slug',$request->slug)->first();
        if($producto){
            return response()->json(['status'=> false],200);
        }
        return response()->json(['status'=> true],200);
    }

    public function updateCurrentImg(Request $request){
        $producto = Producto::withTrashed()->where('id',$request->producto_id)->first();
        if($producto){
            $producto->current_img = $request->current_img;
            $producto->save();
            return response()->json(['status' => true],200);
        } else {
            return response()->json(['status' => false],500);
        }
    }

    public function deleteImg(Request $request){
        $disk = Storage::disk('public');
        if($disk->delete("productos/$request->producto_id/$request->file")){
            return response()->json(['status' => true],200);
        } else {
            return response()->json(['status' => false],500);
        }
    }

    public function uploadImg(Request $request){
        $image = $request->file('croppedImage');
        $disk = Storage::disk('public');
        if($image){
            if($disk->putFile('productos/'.$request->productoId, $image)){
                if(sizeof($disk->allFiles("productos/$request->productoId")) == 1){
                    $producto = Producto::findOrFail($request->productoId);
                    $producto->on_stock = true;
                    $producto->save();
                }
                return response()->json(['status' => true],200);
            } else {
                return response()->json(['status' => false],500);
            }
        } else {
            return response()->json(['status' => false],500);
        }
    }
}
