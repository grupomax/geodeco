<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Storage;
use App\Models\Categoria;
use App\Models\Producto;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Categorias/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Categoria::create($request->all());
        return response()->json(['status'=> true],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($category)
    {
        $disk = Storage::disk('public');
        $categoria = Categoria::select(['id','name'])->where('slug',$category)->first();
        $categorias = Categoria::select(['id','name','slug'])->get();
        if($categoria){
            $productos = Producto::select('*')->where('category_id', $categoria->id)->where('on_stock', true)->get();
            foreach ($productos as $producto){
                if(!$producto->current_img){
                    $aux_image_exist_r = $disk->allFiles("productos/$producto->id");
                    if($aux_image_exist_r){
                        $aux_image_r = $disk->allFiles("productos/$producto->id")[0];
                        $producto->url_img = $disk->url($aux_image_r);
                    }else{
                        $producto->url_img = '/images/Imagen_no_disponible.jpg';
                    }
                }
                else{
                    $producto->url_img = $producto->current_img;
                }
            }
            return Inertia::render('Categoria',[
                'categoria' => $categoria,
                'productos' => $productos,
                'categorias' => $categorias
            ]);
        }
        return Inertia::render('Error',[
            'categorias' => $categorias
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = Categoria::withTrashed()->where('id',$id)->first();
        if($categoria){
            return Inertia::render('Categorias/Edit', [
                'categoria' => $categoria
            ]);
        } else {
            return Inertia::render('Error');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $categoria = Categoria::withTrashed()->findOrFail($request->categoria_id);
        if($categoria){
            $categoria->name = $request->name;
            $categoria->slug = $request->slug;
            $categoria->save();
            return response()->json(['status'=> true],200);
        } else {
            return response()->json(['status'=> false],200);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Categoria::withTrashed()->findOrFail($id);
        if($categoria->forceDelete()){
            return response()->json(['status'=> true],200);
        } else {
            return response()->json(['status'=> false],200);
        }
    }

    public function lists(){
            $categorias = Categoria::withTrashed()->get();
            return Inertia::render('Categorias/Lists', [
                'categorias' => $categorias
            ]);
    }

    public function toggleCategory(Request $request){
        $categoria = Categoria::withTrashed()->findOrFail($request->category_id);
        if($categoria->deleted_at){
            $categoria->restore();
        } else {
            $categoria->delete();
        }
        return response()->json(['status'=> true],200);
    }

    public function canslug(Request $request){
        $categoria = Categoria::withTrashed()->where('slug',$request->slug)->first();
        if($categoria){
            return response()->json(['status'=> false],200);
        }
        return response()->json(['status'=> true],200);
    }
}
