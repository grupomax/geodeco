<?php

namespace App\Http\Controllers;

use App\Mail\Email;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Storage;
use App\Models\Categoria;
use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Inertia\Inertia;


class PrincipalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $disk = Storage::disk('public');
        $categorias = Categoria::select(['id','name','slug'])->get();
        $productos = Producto::select(
            [
                'products.name as producto_name','products.slug as producto_slug','products.current_img as current_img',
                'products.id as producto_id', 'categories.id as categoria_id',
                'categories.name as categoria_name', 'categories.slug as categoria_slug',
                'products.offer as producto_oferta'
        ])->join('categories', 'products.category_id', "=", 'categories.id')
        ->where('products.on_stock', true)
        ->latest('products.created_at')
        ->get();
        foreach ($productos as $producto){
            if(!$producto->current_img){
                $aux_image_exist = $disk->allFiles("productos/$producto->producto_id");
                if($aux_image_exist){
                    $aux_image = $disk->allFiles("productos/$producto->producto_id")[0];
                    $producto->url_img = $disk->url($aux_image);
                }else{
                    $producto->url_img = '/images/Imagen_no_disponible.jpg';
                }
            }
            else{
                $producto->url_img = $producto->current_img;
            }
        }
        return Inertia::render('HeroSection',[
            'categorias' => $categorias,
            'productos' => $productos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function error()
    {
        $categorias = Categoria::select(['id','name','slug'])->get();
        return Inertia::render('Error',[
            'categorias' => $categorias
        ]);
    }

    public function mail(Request $request)
    {
        $consulta = $request->all();
        Mail::to('piolettigeovanafernanda@gmail.com')->send(new Email($consulta));
        return response()->json(["message" => "Email enviado."]);
    }
}
