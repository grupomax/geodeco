import ApplicationLogo from '../Components/ApplicationLogo';
import Dropdown from '../Components/Dropdown';
import NavLink from '../Components/NavLink';
import React, { useState } from 'react';
import ResponsiveNavLink from '../Components/ResponsiveNavLink';
import { InertiaLink } from '@inertiajs/inertia-react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faSignOutAlt, faBorderAll, faChair } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';
import { Inertia } from '@inertiajs/inertia';

library.add(faBars, faSignOutAlt, faBorderAll, faChair);
export default function Authenticated({ auth, header, children }) {
    const [showingNavigationDropdown, setShowingNavigationDropdown] = useState(false);
    const [expandSideBar, setExpandSidebar] = useState(false);
    const claseSideBar = classNames({
        "hidden sm:grid grid-cols-1 grid-rows-14 fixed bg-header-nav shadow-2xl w-20 h-screen z-40 transition ease-out duration-600":true,
        "transform w-20": !expandSideBar,
        "transform w-36": expandSideBar,
    });
    const spanTagSideBar = classNames({
        "inline-block align-middle px-1 py-1 none-outline text-sm font-semibold text-yellow-400 focus:outline-none cursor-pointer":true,
        "hidden": !expandSideBar,
    });
    const categoryTagSideBar = classNames({
        "p-4 w-full rounded-r-full":true,
        "bg-gray-200 bg-opacity-25": route().current('categorias') || route().current('categorias.*'),
    });
    const productosTagSideBar = classNames({
        "p-4 w-full rounded-r-full":true,
        "bg-gray-200 bg-opacity-25": route().current('productos') || route().current('productos.*'),
    });
    const handleExpand = () => {
        setExpandSidebar(!expandSideBar);
    };
    const handleProductos = () => {
        Inertia.get(route('productos'));
    };
    const handleCategorias = () => {
        Inertia.get(route('categorias'));
    };
    const handleLogOut = () => {
        Inertia.post(route('logout'));
    };
    return (
        <div className="min-h-screen backend-gradient">
            <div className={claseSideBar}>
                <div className="flex items-center justify-center cursor-pointer" onClick={handleExpand}>
                    <FontAwesomeIcon icon="bars" style={{color: "#F59E0B"}} size="lg"/>
                </div>
                <div className="flex items-center" onClick={() => handleCategorias()}>
                    <div className={categoryTagSideBar}>
                        <FontAwesomeIcon icon="border-all" className="cursor-pointer ml-3 mr-1" style={{color: "#F59E0B"}} size="lg"/>
                        <span className={spanTagSideBar}>Categorias</span>
                    </div>
                </div>
                <div className="flex items-center justify-center" onClick={() => handleProductos()}>
                    <div className={productosTagSideBar}>
                        <FontAwesomeIcon icon="chair" className="cursor-pointer ml-3 mr-1" style={{color: "#F59E0B"}} size="lg"/>
                        <span className={spanTagSideBar}>Productos</span>
                    </div>
                </div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div className="flex items-center justify-center cursor-pointer" onClick={() => handleLogOut()}>
                    <FontAwesomeIcon icon="sign-out-alt" style={{color: "#F59E0B"}} size="lg"/>
                    <span className={spanTagSideBar}>Salir</span>
                </div>
            </div>
            <nav className="bg-header-nav border-b border-yellow-500">
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="flex justify-between h-16">
                        <div className="flex">
                            <div className="flex-shrink-0 flex items-center">
                                <InertiaLink href="/">
                                    <ApplicationLogo className="block w-40" />
                                </InertiaLink>
                            </div>
                        </div>

                        {/* <div className="hidden sm:flex sm:items-center sm:ml-6">
                            <div className="ml-3 relative">
                                <Dropdown>
                                    <Dropdown.Trigger>
                                        <span className="inline-flex rounded-md">
                                            <button
                                                type="button"
                                                className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-700 bg-geodeco hover:text-gray-500 focus:outline-none transition ease-in-out duration-150"
                                            >
                                                {auth.user.name}

                                                <svg
                                                    className="ml-2 -mr-0.5 h-4 w-4"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 20 20"
                                                    fill="currentColor"
                                                >
                                                    <path
                                                        fillRule="evenodd"
                                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                        clipRule="evenodd"
                                                    />
                                                </svg>
                                            </button>
                                        </span>
                                    </Dropdown.Trigger>

                                    <Dropdown.Content>
                                        <Dropdown.Link href={route('logout')} method="post" as="button">
                                            Salir
                                        </Dropdown.Link>
                                    </Dropdown.Content>
                                </Dropdown>
                            </div>
                        </div> */}

                        <div className="-mr-2 flex items-center sm:hidden">
                            <button
                                onClick={() => setShowingNavigationDropdown((previousState) => !previousState)}
                                className="inline-flex items-center justify-center p-2 rounded-md text-geodeco focus:outline-none transition duration-150 ease-in-out"
                            >
                                <svg className="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                    <path
                                        className={!showingNavigationDropdown ? 'inline-flex' : 'hidden'}
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        d="M4 6h16M4 12h16M4 18h16"
                                    />
                                    <path
                                        className={showingNavigationDropdown ? 'inline-flex' : 'hidden'}
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        d="M6 18L18 6M6 6l12 12"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div className={(showingNavigationDropdown ? 'block' : 'hidden') + ' sm:hidden'}>
                    <div className="pt-2 pb-3 space-y-1">
                        <ResponsiveNavLink
                            href={route('productos')}
                            active={route().current('productos') || route().current('productos.*')}
                        >
                            Productos
                        </ResponsiveNavLink>
                        <ResponsiveNavLink
                            href={route('categorias')}
                            active={route().current('categorias') || route().current('categorias.*')}
                        >
                            Categorías
                        </ResponsiveNavLink>
                    </div>

                    <div className="pt-4 pb-1 border-t border-gray-200">
                        <div className="mt-3 space-y-2">
                            <ResponsiveNavLink method="post" href={route('logout')} as="button">
                                Salir
                            </ResponsiveNavLink>
                        </div>
                    </div>
                </div>
            </nav>



            <main>{children}</main>
        </div>
    );
}
