import {useState, useEffect} from 'react';
import axios from 'axios';
const ValidateForm = (callback, validate) => {
    const [values, setValues] = useState({
        name: '',
        email: '',
        contact_from: '',
        area_code: '',
        phone: '',
        comment: '',
    });

    const [errors,setErrors] = useState({});
    const[isSubmitting, setIsSubmitting] = useState(false);


    const handleChange = e => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = e => {
        e.preventDefault();

        setErrors(validate(values));
        setIsSubmitting(true);
    }

    useEffect(() => {
        if(Object.keys(errors).length === 0 && isSubmitting){
            var botn = document.querySelector('.form-input-btn');
            botn.disabled = true;
            botn.innerHTML = "<div class='spinner'>"+"</div>";
            const contact={};
            switch(values.contact_from){
                case "1":
                    contact.from = "10:00:00";
                    contact.to = "12:00:00";
                    break;

                case "2":
                    contact.from = "12:00:00";
                    contact.to = "15:00:00";
                    break;

                case "3":
                    contact.from = "15:00:00";
                    contact.to = "19:00:00";
                    break;

                default:
                    contact.from = "10:00:00";
                    contact.to = "19:00:00";
            }
            var urlencoded = new FormData();
            urlencoded.append("_token", document.querySelector('meta[name="csrf-token"]').content);
            urlencoded.append("nombre_apellido", values.name);
            urlencoded.append("email", values.email);
            urlencoded.append("telefono", values.area_code+values.phone);
            urlencoded.append("from", contact.from);
            urlencoded.append('to', contact.to);
            urlencoded.append("comentario", values.comment);

            axios.post("api/send/email", urlencoded)
            .then(() => callback())
            .catch(error => console.log('error', error));
        };
    });

    return {handleChange, values, handleSubmit, errors }
}

export default ValidateForm

