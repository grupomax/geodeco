import React from 'react';
import "./FormSuccess.css";

const FormSuccess = () => {
    return (
        <div className="form-content-success">
            <div className="form-success">
                <h5 className="form-success-title">¡Gracias por confiar <br></br> en nosotros!</h5>
                <p className="form-success-text">En breve un asesor comercial se <br></br> pondrá en contacto para brindarle <br></br> más información.</p>
            </div>
        </div>


    )
}

export default FormSuccess
