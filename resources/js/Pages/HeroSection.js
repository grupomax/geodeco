import React from 'react';
import Navbar from './Navbar';
import Slider from './Slider';
import Nosotros from './Nosotros';
import ProductoItem from './ProductoItem';
import Footer from './Footer/Footer';

function HeroSection(props){
    const categorias = props.categorias;
    const productos = props.productos;
    return (
        <>
            <Navbar categorias={categorias} />
            <Slider />
            <Nosotros />
            <ProductoItem productos={productos}/>
            <Footer/>
        </>
    )
}

export default HeroSection
