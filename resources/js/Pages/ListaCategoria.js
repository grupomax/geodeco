import React, {useState} from "react";
import producto from "../../images/banners/Producto.jpg";
import producto_mobile from "../../images/banners/Producto_mobile.jpg";
import ProductCategory from "./ProductCategory";
import {slice, concat} from 'lodash';

function ListaCategoria(props) {
    const categoria = props.categoria;
    const Productos = props.productos;

    const LENGTH = Productos.length;
    const DATA = [ ...Array(LENGTH).keys() ];
    const IMAGE_SRC="https://source.unsplash.com/random";
    const LIMIT = 3;

    const [showMore,setShowMore] = useState(true);
    const [list,setList] = useState(slice(Productos, 0, LIMIT))
    const [index,setIndex] = useState(LIMIT);

    const loadMore = () =>{
        const newIndex = index + LIMIT;
        const newShowMore = newIndex < (LENGTH - 1);
        const newList = concat(list, slice(Productos, index, newIndex));
        setIndex(newIndex);
        setList(newList);
        setShowMore(newShowMore);
    }
        return (
        <>
            <div className="cards" id="lista_categoria">
                <div className="cards__container">
                    <div className="cont-title-sections">
                    <img src={window.innerWidth <= 768 ? producto_mobile : producto} alt="producto banner" className="producto__banner"/>
                    <p className="titulo__producto__banner">{categoria.name}</p>
                    </div>
                    <div className="container__productos">
                    <div className="image__container">
                        {list.map((elemento)=>
                            <ProductCategory producto={elemento} key={elemento.id}/>
                        )}
                    </div>
                    </div>
                    <div className="boton__producto__content">
                    {(LENGTH >'3') ? showMore && <button onClick={loadMore} className="show_more_btn"> VER MÁS PRODUCTOS</button> : ''}

                </div>
                </div>
            </div>
        </>
    )
}

export default ListaCategoria
