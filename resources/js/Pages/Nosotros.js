import React from "react";
import Collapse from "@kunukn/react-collapse";
import nosotros from "../../images/iconos/nosotros.png";

function Empresa() {
    const [isOpen3, setIsOpen3] = React.useState(false);
    const [isOpen4, setIsOpen4] = React.useState(false);

    const onInit3 = ({ state, style, node }) => {
        setIsOpen3(true);
    };
    const onInit4 = ({ state, style, node }) => {
        setIsOpen4(true);
    };
    return (
        <div className="cards" id="empresa">
            <div className="cards__wrapper">
                <div className="tu__empresa empresa__somos">
                <img src={nosotros} alt="nosotros_banner" className="nosotros__banner"/>
                </div>
                <div className="tu__empresa empresa__elegir">
                    <h1 className="nosotros__titulo">Nosotros</h1>
                    <p className="nosotros__texto">
                        Nos dedicamos al diseño y restauración de muebles a pedido y a tu gusto, para que sean únicos, irrepetibles y funcionales.<br></br>
                        Nuestros diseños nacen de la imaginación y la pasión que sentimos al crearlos. Buscamos generar piezas únicas en tu espacio, priorizando sobre todo el confort.<br></br>
                        Trabajamos muebles con espejos, con pintura glitter y con resina epóxica.
                    </p>
                </div>
            </div>
        </div>
    );
}

export default Empresa;
