import Authenticated from '@/Layouts/Authenticated';
import React from 'react';

export default function Productos(props) {
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
        >
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-gray-200 bg-opacity-50 overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 border-b border-gray-200">Productos</div>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
