import React from "react";
import Navbar from "./NavbarModel";
import Footer from "./Footer/FooterModel";
import ProductoView from "./ProductoView";

function Producto(props) {
    const categorias = props.categorias;
    const producto = props.producto;
    const imagenes = props.imagenes;
    const recomendados = props.recomendados;
    return (
        <>
        <Navbar categorias={categorias}/>
        <ProductoView producto={producto} imagenes={imagenes} recomendados={recomendados}/>
        <Footer />
        </>
    );
}
export default Producto;
