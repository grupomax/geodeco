import React from "react";
import CardItem from "./ModelItem";
import producto from "../../images/banners/Producto.jpg";

const MODELO4010 = [
  "../images/modelos/Luxury_4010/Luxury_4010_1.jpg",
  "../images/modelos/Luxury_4010/Luxury_4010_2.jpg",
];

const MODELO4020 = [
  "../images/modelos/Luxury_4020/Luxury_4020_1.jpg",
  "../images/modelos/Luxury_4020/Luxury_4020_2.jpg",
  "../images/modelos/Luxury_4020/Luxury_4020_3.jpg",
  "../images/modelos/Luxury_4020/Luxury_4020_4.jpg",
];

const MODELO4030 = [
  "../images/modelos/Luxury_4030/Luxury_4030_1.jpg",
  "../images/modelos/Luxury_4030/Luxury_4030_2.jpg",
  "../images/modelos/Luxury_4030/Luxury_4030_3.jpg",
  "../images/modelos/Luxury_4030/Luxury_4030_4.jpg",
];

const MODELO4040 = [
  "../images/modelos/Luxury_4040/Luxury_4040_1.jpg",
  "../images/modelos/Luxury_4040/Luxury_4040_2.jpg",
  "../images/modelos/Luxury_4040/Luxury_4040_3.jpg",
  "../images/modelos/Luxury_4040/Luxury_4040_4.jpg",
];

const MODELO4050 = [
  "../images/modelos/Luxury_4050/Luxury_4050_1.jpg",
  "../images/modelos/Luxury_4050/Luxury_4050_2.jpg",
  "../images/modelos/Luxury_4050/Luxury_4050_3.jpg",
  "../images/modelos/Luxury_4050/Luxury_4050_4.jpg",
];

const MODELO4060 = [
  "../images/modelos/Luxury_4060/Luxury_4060_1.jpg",
  "../images/modelos/Luxury_4060/Luxury_4060_2.jpg",
  "../images/modelos/Luxury_4060/Luxury_4060_3.jpg",
];

const MODELO5010 = [
  "../images/modelos/Palace_5010/Palace_5010_1.jpg",
  "../images/modelos/Palace_5010/Palace_5010_2.jpg",
  "../images/modelos/Palace_5010/Palace_5010_3.jpg",
];

const MODELO5020 = [
  "../images/modelos/Palace_5020/Palace_5020_1.jpg",
  "../images/modelos/Palace_5020/Palace_5020_2.jpg",
  "../images/modelos/Palace_5020/Palace_5020_3.jpg",
  "../images/modelos/Palace_5020/Palace_5020_4.jpg",
];

const MODELO5030 = [
  "../images/modelos/Palace_5030/Palace_5030_1.jpg",
  "../images/modelos/Palace_5030/Palace_5030_2.jpg",
  "../images/modelos/Palace_5030/Palace_5030_3.jpg",
  "../images/modelos/Palace_5030/Palace_5030_4.jpg",
];

const MODELO5040 = [
  "../images/modelos/Palace_5040/Palace_5040_1.jpg",
  "../images/modelos/Palace_5040/Palace_5040_2.jpg",
  "../images/modelos/Palace_5040/Palace_5040_3.jpg",
];

const items_1 = [
  {
    src: "../images/modelos/Luxury_4010/Luxury_4010_1.jpg",
    text: "Explore the hidden waterfall deep inside the Amazon jungle",
    label: "Luxury 4010",
    path: "/modelo/4010",
    camas: "2",
    duchas: "2",
    metros_cubierto: "102",
    metros_semicubierto: "17",
    entrega: "50",
    cuotas: "60",
    plantas: "1",
    contenedor: MODELO4010,
  },
  {
    src: "../images/modelos/Luxury_4020/Luxury_4020_1.jpg",
    text: "Explore the hidden waterfall deep inside the Amazon jungle",
    label: "Luxury 4020",
    path: "/modelo/4020",
    camas: "3",
    duchas: "2",
    metros_cubierto: "140",
    metros_semicubierto: "25",
    entrega: "50",
    cuotas: "60",
    plantas: "1",
    contenedor: MODELO4020,
  }
];

const items_2 = [
  {
    src: "../images/modelos/Luxury_4030/Luxury_4030_1.jpg",
    text: "Explore the hidden waterfall deep inside the Amazon jungle",
    label: "Luxury 4030",
    path: "/modelo/4030",
    camas: "2",
    duchas: "2",
    metros_cubierto: "116",
    metros_semicubierto: "58",
    entrega: "50",
    cuotas: "60",
    plantas: "1",
    contenedor: MODELO4030,
  },
  {
    src: "../images/modelos/Luxury_4040/Luxury_4040_1.jpg",
    text: "Explore the hidden waterfall deep inside the Amazon jungle",
    label: "Luxury 4040",
    path: "/modelo/4040",
    camas: "3",
    duchas: "2",
    metros_cubierto: "107",
    metros_semicubierto: "17",
    entrega: "50",
    cuotas: "60",
    plantas: "1",
    contenedor: MODELO4040,
  }
];

const items_3 = [
  {
    src: "../images/modelos/Luxury_4050/Luxury_4050_1.jpg",
    text: "Explore the hidden waterfall deep inside the Amazon jungle",
    label: "Luxury 4050",
    path: "/modelo/4050",
    camas: "2",
    duchas: "2",
    metros_cubierto: "85",
    metros_semicubierto: "25",
    entrega: "50",
    cuotas: "60",
    plantas: "1",
    contenedor: MODELO4050,
  },
  {
    src: "../images/modelos/Luxury_4060/Luxury_4060_1.jpg",
    text: "Explore the hidden waterfall deep inside the Amazon jungle",
    label: "Luxury 4060",
    path: "/modelo/4060",
    camas: "3",
    duchas: "2",
    metros_cubierto: "107",
    metros_semicubierto: "0",
    entrega: "50",
    cuotas: "60",
    plantas: "1",
    contenedor: MODELO4060,
  }
];

const items_4 = [
  {
    src: "../images/modelos/Palace_5010/Palace_5010_1.jpg",
    text: "Explore the hidden waterfall deep inside the Amazon jungle",
    label: "Palace 5010",
    path: "/modelo/5010",
    camas: "3",
    duchas: "3",
    metros_cubierto: "248",
    metros_semicubierto: "0",
    entrega: "50",
    cuotas: "60",
    plantas: "2",
    contenedor: MODELO5010,
  },
  {
    src: "../images/modelos/Palace_5020/Palace_5020_1.jpg",
    text: "Explore the hidden waterfall deep inside the Amazon jungle",
    label: "Palace 5020",
    path: "/modelo/5020",
    camas: "2",
    duchas: "3",
    metros_cubierto: "416.55",
    metros_semicubierto: "0",
    entrega: "50",
    cuotas: "60",
    plantas: "2",
    contenedor: MODELO5020,
  }
];

const items_5 = [
  {
    src: "../images/modelos/Palace_5030/Palace_5030_1.jpg",
    text: "Explore the hidden waterfall deep inside the Amazon jungle",
    label: "Palace 5030",
    path: "/modelo/5030",
    camas: "3",
    duchas: "3",
    metros_cubierto: "204.44",
    metros_semicubierto: "58.17",
    entrega: "50",
    cuotas: "60",
    plantas: "2",
    contenedor: MODELO5030,
  },
  {
    src: "../images/modelos/Palace_5040/Palace_5040_1.jpg",
    text: "Explore the hidden waterfall deep inside the Amazon jungle",
    label: "Palace 5040",
    path: "/modelo/5040",
    camas: "3",
    duchas: "3",
    metros_cubierto: "239.78",
    metros_semicubierto: "26.91",
    entrega: "50",
    cuotas: "60",
    plantas: "2",
    contenedor: MODELO5040,
  }
];
function Models() {
  return (
    <div className="cards" id="models">
      <div className="cards__container">
        <div className="cont-title-sections">
        <img src={producto} alt="producto banner" className="producto__banner"/>
        <p className="titulo__producto__banner">NOVEDOSOS Y BRILLANTES</p>
        </div>
        <div className="cards__wrapper">
          <ul className="cards__items">
            {items_1.map((item, i) => (
              <CardItem
                key={i}
                src={item.src}
                text={item.text}
                label={item.label}
                path={item.path}
                camas={item.camas}
                duchas={item.duchas}
                metros_cubierto={item.metros_cubierto}
                metros_semicubierto={item.metros_semicubierto}
                entrega={item.entrega}
                cuotas={item.cuotas}
                plantas={item.plantas}
                contenedor={item.contenedor}
              />
            ))}
          </ul>
          <ul className="cards__items">
            {items_2.map((item, i) => (
              <CardItem
                key={i}
                src={item.src}
                text={item.text}
                label={item.label}
                path={item.path}
                camas={item.camas}
                duchas={item.duchas}
                metros_cubierto={item.metros_cubierto}
                metros_semicubierto={item.metros_semicubierto}
                entrega={item.entrega}
                cuotas={item.cuotas}
                plantas={item.plantas}
                contenedor={item.contenedor}
                numero={item.numero}
                />
            ))}
          </ul>
          <ul className="cards__items">
            {items_3.map((item, i) => (
              <CardItem
                key={i}
                src={item.src}
                text={item.text}
                label={item.label}
                path={item.path}
                camas={item.camas}
                duchas={item.duchas}
                metros_cubierto={item.metros_cubierto}
                metros_semicubierto={item.metros_semicubierto}
                entrega={item.entrega}
                cuotas={item.cuotas}
                plantas={item.plantas}
                contenedor={item.contenedor}
                numero={item.numero}
                />
            ))}
          </ul>
          <ul className="cards__items">
            {items_4.map((item, i) => (
              <CardItem
                key={i}
                src={item.src}
                text={item.text}
                label={item.label}
                path={item.path}
                camas={item.camas}
                duchas={item.duchas}
                metros_cubierto={item.metros_cubierto}
                metros_semicubierto={item.metros_semicubierto}
                entrega={item.entrega}
                cuotas={item.cuotas}
                plantas={item.plantas}
                contenedor={item.contenedor}
                numero={item.numero}
                />
            ))}
          </ul>
          <ul className="cards__items">
            {items_5.map((item, i) => (
              <CardItem
                key={i}
                src={item.src}
                text={item.text}
                label={item.label}
                path={item.path}
                camas={item.camas}
                duchas={item.duchas}
                metros_cubierto={item.metros_cubierto}
                metros_semicubierto={item.metros_semicubierto}
                entrega={item.entrega}
                cuotas={item.cuotas}
                plantas={item.plantas}
                contenedor={item.contenedor}
                numero={item.numero}
                />
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Models;
