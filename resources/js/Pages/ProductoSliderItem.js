import React from 'react';

function ProductoSliderItem(props) {
    return (
        <>
            <img src={props.src} key={props.llave} alt="sliders" className="slider_producto" />
        </>
    )
}

export default ProductoSliderItem
