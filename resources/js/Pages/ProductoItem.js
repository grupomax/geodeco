import React, {useState} from "react";
import ProductHome from "./ProductHome";
import producto from "../../images/banners/Producto.jpg";
import producto_mobile from "../../images/banners/Producto_mobile.jpg";
import {slice, concat} from 'lodash';

function ProductoItem(props) {
    const Productos = props.productos;

    const LENGTH = Productos.length;
    const DATA = [ ...Array(LENGTH).keys() ];
    const IMAGE_SRC="https://source.unsplash.com/random";
    const LIMIT = 3;

const [showMore,setShowMore] = useState(true);
const [list,setList] = useState(slice(Productos, 0, LIMIT))
const [index,setIndex] = useState(LIMIT);

const loadMore = () =>{
    const newIndex = index + LIMIT;
    const newShowMore = newIndex < (LENGTH - 1);
    const newList = concat(list, slice(Productos, index, newIndex));
    setIndex(newIndex);
    setList(newList);
    setShowMore(newShowMore);
}

return (
    <>
        <div className="cards" id="models">
            <div className="cards__container">
                <div className="cont-title-sections">
                <img src={window.innerWidth <= 768 ? producto_mobile : producto} alt="producto banner" className="producto__banner"/>
                <p className="titulo__producto__banner">NOVEDOSOS {window.innerWidth <= 768 ?<br></br>: ''}Y BRILLANTES</p>
                </div>
                <div className="container__productos">
                    <div className="image__container">
                        {list.map((elemento)=>
                            <ProductHome producto={elemento} key={elemento.producto_id}/>
                        )}
                    </div>
                </div>
                <div className="boton__producto__content">
                {(LENGTH >'3') ? showMore && <button onClick={loadMore} className="show_more_btn"> VER MÁS </button> : ''}
                </div>
            </div>
        </div>
    </>
  );
}
export default ProductoItem;
