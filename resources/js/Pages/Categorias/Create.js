import Authenticated from '@/Layouts/Authenticated';
import React from 'react';
import CreateSlugCategory from '@/Components/CreateSlugCategory';

export default function Edit(props) {
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
        >
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 bg-white border-b border-gray-200">
                            <h3 className="text-xl font-semibold py-4">Nueva Categoría</h3>
                        </div>
                        <CreateSlugCategory />
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
