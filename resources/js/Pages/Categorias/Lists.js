import Authenticated from '@/Layouts/Authenticated';
import React from 'react';
import { InertiaLink } from '@inertiajs/inertia-react';
import TrCategory from '@/Components/TrCategory';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faBatteryEmpty } from '@fortawesome/free-solid-svg-icons';

library.add(faPlus, faBatteryEmpty);
export default function Categorias(props) {
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
        >
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 bg-white border-b border-gray-200 flex justify-between">
                            <h3 className="text-xl font-semibold">Categorías</h3>
                            <InertiaLink
                                href={route('categorias.create')}
                                className="pull-right inline-flex items-center px-4 py-2 bg-geodeco border border-transparent rounded-md font-semibold text-xs text-gray-600 uppercase tracking-widest active:bg-gray-900 transition ease-in-out duration-150"
                            >
                                <FontAwesomeIcon icon="plus" className="mr-2"/>
                                Nueva Categoría
                            </InertiaLink>
                        </div>
                        <table className="min-w-full divide-y divide-gray-200">
                            <thead className="bg-gray-100">
                                <tr>
                                <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Nombre
                                </th>
                                <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Estado
                                </th>
                                <th scope="col" className="relative px-6 py-3">
                                    <span className="sr-only">Edit</span>
                                </th>
                                </tr>
                            </thead>
                            <tbody className="bg-white divide-y divide-gray-200" id="categorias-table">
                                {props.categorias.map((item) =>
                                    <TrCategory key={item.id} categoria={item} />
                                )}
                                {!props.categorias.length &&
                                    <tr>
                                        <td colSpan="3">
                                            <div className="w-full h-64 flex items-center justify-center">
                                                <div className="flex flex-col items-center font-extrabold text-xl text-gray-400">
                                                    <FontAwesomeIcon icon="battery-empty" size="lg"/>
                                                    <span>No hay Categorías aún...</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
