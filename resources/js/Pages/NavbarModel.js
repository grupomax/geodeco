    import React, { useState, useEffect } from "react";
    import { BsList, BsX } from "react-icons/bs";
    import { IconContext } from "react-icons/lib";
    import { FaFacebookSquare, FaInstagram, FaWhatsapp } from "react-icons/fa";
    import logo_claro from "../../images/iconos/geodeco-logo.png";
    import NavLink from '../Components/NavLink';
    import { InertiaLink } from '@inertiajs/inertia-react';
    import { Link } from "react-scroll";

    function NavBarModel(props) {
    const [click, setClick] = useState(false);
    const [button, setButton] = useState(true);
    const [navbar, setNavBar] = useState(false);

    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    const showButton = () => {
        if (window.innerWidth <= 960) {
        setButton(false);
        } else {
        setButton(true);
        }
    };

    const categorias = props.categorias;

    useEffect(() => {
        showButton();
    }, []);

    window.addEventListener("resize", showButton);

    const changeBackground = () => {
        if (window.scrollY >= 0.1) {
        setNavBar(true);
        } else {
        setNavBar(false);
        }
    };

    window.addEventListener("scroll", changeBackground);

    return (
        <>
        <IconContext.Provider value={{ color: "#373334" }}>
            <div className={`navbar ${navbar && "active"}`} id="navbar__model">
            <div className="navbar-container container">
                <InertiaLink href="/" onClick={closeMobileMenu} className="navbar-logo">
                    <img src={logo_claro} className="navbar-logo-icon" alt="" />
                </InertiaLink>
                <div className="menu-icon" onClick={handleClick}>
                {click ? <BsX /> : <BsList />}
                </div>
                <ul className={click ? "nav-menu active" : "nav-menu"}>
                <li className="nav-item"  id="inicializado">
                <InertiaLink href="/" onClick={closeMobileMenu} className="nav-links">
                    Inicio
                </InertiaLink>
                </li>
                <li className="nav-item" id="dropdown_modelo">
                {window.innerWidth <= 960 ?
                    <Link
                    to=''
                    className="nav-links"
                    id="btn__models"
                    >
                    Productos
                    </Link>
                    :
                    <InertiaLink
                    className="nav-links"
                    href="/#models"
                    id="btn__models"
                    onClick={closeMobileMenu}
                    >
                    Productos
                    </InertiaLink>
                }
                  <ul className="lista__modelos">
                        {categorias.map((categoria) => (
                            <li key={categoria.id}>
                                <NavLink
                                href={route("categoria",categoria.slug)}
                                onClick={
                                    closeMobileMenu
                                }
                                >
                                {categoria.name}
                                </NavLink>
                            </li>
                        ))}
                    </ul>
                </li>
                <li className="nav-item" id="contacten">
                    <InertiaLink
                    className="nav-links"
                    href="/#contact"
                    onClick={closeMobileMenu}
                    >
                    Contacto
                    </InertiaLink>
                </li>
              </ul>
            </div>
          </div>
        </IconContext.Provider>
      </>
    );
    }

    export default NavBarModel;
