import React, { useState } from "react";
import { IoLogoWhatsapp } from "react-icons/io";
import { ImPhone } from "react-icons/im";
import { RiMailSendLine } from "react-icons/ri";
import Form from "../Form/Form";
import FormSuccess from "../Form/FormSuccess";
import { Link} from "react-scroll";
import NavLink from '../../Components/NavLink';
import { FaFacebookSquare, FaInstagram, FaWhatsapp } from "react-icons/fa";

function Footer() {
  const [isSubmitted, setIsSubmitted] = useState(false);

  function submitForm() {
    setIsSubmitted(true);
  }

  return (
    <div className="footer-container" id="contact">
      <div className="footer-links">
        <div className="footer-link-wrapper1">
          <div className="footer-link-items">
            {!isSubmitted ? <Form submitForm={submitForm} /> : <FormSuccess />}
            {/* <FormSuccess/> */}
          </div>
        </div>
        <div className="footer-link-wrapper2">
          <div className="footer-link-items">
          <h2>Contacto</h2>
            <a
              href="https://api.whatsapp.com/send?phone=5493513433653&text=Hola,%20quisiera%20realizar%20una%20consulta."
              target="_blank" rel="noreferrer"
            >
              <IoLogoWhatsapp className="footer_icon_social" /> 351-3433653
            </a>
            <a href="https://www.instagram.com/geodeco13/" target="_blank" rel="noreferrer">
              <FaInstagram className="footer_icon_social" /> Instagram
            </a>
            <a href="mailto:piolettigeovanafernanda@gmail.com" target="_blank"><RiMailSendLine className="footer_icon_social" />piolettigeovanafernanda@gmail.com</a>
          </div>
        </div>
      </div>
      <div className="footer-coopiri">
        <NavLink
          href={route("login")}
          className="footer-text-coopiri">
          Todos los derechos reservados
        </NavLink>
      </div>
    </div>
  );
}

export default Footer;
