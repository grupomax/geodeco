import Authenticated from '@/Layouts/Authenticated';
import React from 'react';
import ProductoItem from '@/Components/ProductoItem';
import { InertiaLink } from '@inertiajs/inertia-react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faBatteryEmpty } from '@fortawesome/free-solid-svg-icons';

library.add(faPlus, faBatteryEmpty);
export default function ProductosList(props) {
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
        >
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 bg-white border-b border-gray-200 flex justify-between">
                            <h3 className="text-xl font-semibold">Productos</h3>
                            <InertiaLink
                                href={route('productos.create')}
                                className={`pull-right inline-flex items-center px-4 py-2 bg-geodeco border border-transparent rounded-md font-semibold text-xs text-gray-600 uppercase tracking-widest active:bg-gray-900 transition ease-in-out duration-150 ${!props.categorias?'hidden':''}`}
                            >
                                <FontAwesomeIcon icon="plus" className="mr-2"/>
                                Nuevo Producto
                            </InertiaLink>
                        </div>
                        <div className="grid grid-cols-6 bg-gray-100">
                            <div className="col-span-2 px-2 sm:px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Nombre
                            </div>
                            <div className="hidden sm:flex px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Categoría
                            </div>
                            <div className="hidden sm:flex px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Precio
                            </div>
                            <div className="col-span-2 sm:col-span-1 px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Estado
                            </div>
                            <div className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                <span className="sr-only">Edit</span>
                            </div>
                            {props.productos.map((item) =>
                                    <ProductoItem key={item.id} producto={item} className="col-span-6" />
                            )}
                            {!props.productos.length &&
                                <div className="col-span-6 w-full h-64 flex items-center justify-center">
                                    <div className="flex flex-col items-center font-extrabold text-xl text-gray-400">
                                        <FontAwesomeIcon icon="battery-empty" size="lg"/>
                                        <span>No hay Productos aún... Primero crea categorías y luego productos.</span>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
