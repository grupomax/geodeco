import React, { useState, useEffect } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    useLocation,
    withRouter,
} from "react-router-dom";
import { BsList, BsX } from "react-icons/bs";
import { Link as Linke } from "react-router-dom";
import { Link as Link } from "react-scroll";
import { IconContext } from "react-icons/lib";
import NavLink from '../Components/NavLink';
import { InertiaLink } from '@inertiajs/inertia-react';

import logo_claro from "../../images/iconos/geodeco-logo.png";
import { FaFacebookSquare, FaInstagram, FaWhatsapp } from "react-icons/fa";

function NavBar(props) {
    const [click, setClick] = useState(false);
    const [button, setButton] = useState(true);
    const [navbar, setNavBar] = useState(false);
    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    const showButton = () => {
        if (window.innerWidth <= 960) {
            setButton(false);
        } else {
            setButton(true);
        }
    };

    const categorias = props.categorias;

    useEffect(() => {
        showButton();
        var pathy = window.location.href;

        if (pathy.includes("#")) {
            let pathy2 = pathy.split("/")[3];
            let pathy3 = pathy2.replace("#", "");
            const element = document.querySelector("#btn__" + pathy3);
            setTimeout(() => {
                element.click();
            }, 200);
        }
    }, []);

    window.addEventListener("resize", showButton);

    const changeBackground = () => {
        if (window.scrollY >= 0.1) {
            setNavBar(true);
        } else {
            setNavBar(false);
        }
    };

    window.addEventListener("scroll", changeBackground);

    return (
        <>
            <IconContext.Provider
                value={{
                    color: "#373334",
                }}
            >
                <div className={`navbar ${navbar && "active"}`}>
                    <div className="navbar-container container">
                        <Router>
                            <Link
                                className="navbar-logo"
                                to="inicie"
                                spy={true}
                                smooth={true}
                                offset={-180}
                                duration={600}
                                onClick={closeMobileMenu}
                            >
                                <img
                                    src={logo_claro}
                                    className="navbar-logo-icon"
                                    alt=""
                                />
                            </Link>
                            <div className="menu-icon" onClick={handleClick}>
                                {click ? <BsX /> : <BsList />}
                            </div>
                            <ul
                                className={
                                    click ? "nav-menu active" : "nav-menu"
                                }
                            >
                                <li className="nav-item" id="inicializado">
                                    <Link
                                        to="inicie"
                                        spy={true}
                                        smooth={true}
                                        offset={-90}
                                        duration={600}
                                        className="nav-links"
                                        onClick={closeMobileMenu}
                                    >
                                        Inicio
                                    </Link>
                                </li>
                                <li
                                    className="nav-item"
                                    id="dropdown_modelo"
                                >
                                    {window.innerWidth <= 960 ?
                                    <Link
                                    to=''
                                    className="nav-links"
                                    id="btn__models"
                                    >
                                    Productos
                                    </Link>
                                    :
                                    <Link
                                        to="models"
                                        spy={true}
                                        smooth={true}
                                        offset={-10}
                                        duration={600}
                                        className="nav-links"
                                        onClick={closeMobileMenu}
                                        id="btn__models"
                                    >
                                        Productos
                                    </Link>
                                    }
                                    <ul className="lista__modelos">
                                        {categorias.map((categoria) => (
                                            <li key={categoria.id}>
                                                <NavLink
                                                    href={route("categoria",categoria.slug)}
                                                    onClick={
                                                        closeMobileMenu
                                                    }
                                                >
                                                    {categoria.name}
                                                </NavLink>
                                            </li>
                                        ))}
                                    </ul>
                                </li>
                                <li className="nav-item" id="contacten">
                                    <Link
                                        to="contact"
                                        spy={true}
                                        smooth={true}
                                        offset={-50}
                                        duration={600}
                                        className="nav-links"
                                        onClick={closeMobileMenu}
                                        id="btn__contact"
                                    >
                                        Contacto
                                    </Link>
                                </li>
                                
                            </ul>
                        </Router>
                    </div>
                </div>
            </IconContext.Provider>
        </>
    );
}

export default NavBar;
