import React from 'react';
import { InertiaLink } from '@inertiajs/inertia-react';
import Recomendados from './Recomendados';
import ProductoShow from './ProductoShow';

function ProductoView(props) {
    const producto = props.producto;
    const imagenes = props.imagenes;
    const recomendados = props.recomendados;
    return (
        <>
        <ProductoShow producto={producto} imagenes={imagenes}/>
        <Recomendados recomendados={recomendados}/>
        </>
    )
}

export default ProductoView
