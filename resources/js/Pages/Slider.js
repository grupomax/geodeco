import React from "react";
import { Carousel } from "react-responsive-carousel";
import { FaFacebookSquare, FaInstagram, FaWhatsapp } from "react-icons/fa";
import { Link } from "react-scroll";
import { Link as Linke } from "react-router-dom";
import banner1 from "../../images/banners/Principal.jpg";
import banner1_mobile from "../../images/banners/Principal_mobile.jpg";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

function Slider() {
  return (
    <>
      <Carousel
        showArrows={false}
        emulateTouch={true}
        showThumbs={false}
        showStatus={false}
        showIndicators={false}
        infiniteLoop={true}
        swipeable={true}
        useKeyboardArrows={true}
        margin={0}
        padding={0}
        autoPlay={true}
        interval={5000}
        transitionTime={800}
      >
        <div id="inicie">
          <img src={window.innerWidth <= 768 ? banner1_mobile : banner1} alt="slider1" className="slider__home" />
          <div className="slider-texto">
            <p className="primer__text">MARCA TU</p>
            <h1 className="segundo__text">ESTILO PROPIO</h1>
          </div>
          {/* <img src={window.innerWidth <= 960 ? mobile1 : pc1} alt="slider1" className="slider__home" /> */}
        </div>
      </Carousel>
    </>
  );
}

export default Slider;
