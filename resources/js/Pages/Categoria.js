import React from "react";
import NavBar from "./NavbarModel";
import Footer from "./Footer/FooterModel";
import ListaCategoria from "./ListaCategoria";

function Categoria(props) {
    const categoria = props.categoria;
    const categorias = props.categorias;
    const productos = props.productos;
    return (
        <>
        <NavBar categorias={categorias}/>
        <ListaCategoria categoria={categoria} productos={productos}/>
        <Footer />
        </>
    );
}
export default Categoria;
