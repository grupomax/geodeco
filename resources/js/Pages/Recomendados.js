import React,{useState} from 'react';
import { InertiaLink } from '@inertiajs/inertia-react';
import {slice, concat} from 'lodash';
import ProductHome from "./ProductHome";

function Recomendados(props) {
    const Productos = props.recomendados;
    const LENGTH = Productos.length;
    const DATA = [ ...Array(LENGTH).keys() ];
    const IMAGE_SRC="https://source.unsplash.com/random";
    const LIMIT = 3;

    const [showMore,setShowMore] = useState(true);
    const [list,setList] = useState(slice(Productos, 0, LIMIT))
    const [index,setIndex] = useState(LIMIT);

    const loadMore = () =>{
        const newIndex = index + LIMIT;
        const newShowMore = newIndex < (LENGTH - 1);
        const newList = concat(list, slice(Productos, index, newIndex));
        setIndex(newIndex);
        setList(newList);
        setShowMore(newShowMore);
    }

    return (
        <>
        <div className="cards" id="products_recomendados">
            <div className="cards__container">
                <div className="cont-title-sections">
                <p className="titulo__producto__recomendado">ESTOS PRODUCTOS PODRÍAN INTERESARTE</p>
                </div>
                <div className="container__productos">
                    <div className="image__container">
                        {list.map((elemento)=>
                            <ProductHome producto={elemento} key={elemento.producto_id}/>
                        )}
                    </div>
                </div>
                <div className="boton__producto__content">
                {(LENGTH >'3') ? showMore && <button onClick={loadMore} className="show_more_btn"> VER MÁS </button> : ''}
                </div>
            </div>
        </div>
        </>
    )
}

export default Recomendados
