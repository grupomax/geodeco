import React from 'react';
import Navbar from "./NavbarModel";
import img_error from "../../images/Error_404.png";
import { InertiaLink } from '@inertiajs/inertia-react';

function Error(props) {
    const categorias = props.categorias;

    return (
        <>
            <Navbar categorias={categorias} />
            <div className="contenedor__error">
                <div className="aviso__error">
                    <h1 className="title__error">Ups!</h1>
                    <p className="subtitle_error">No encontramos la página que estabas buscando.</p>
                    <p className="subtitle_error">Te invitamos a que sigas explorando nuestro sitio.</p>
                    <InertiaLink href="/"
                    className="error_btn">
                        VOLVER
                    </InertiaLink>
                </div>
                <img src={img_error} alt="error" className="img__error"/>
            </div>
        </>
    )
}

export default Error
