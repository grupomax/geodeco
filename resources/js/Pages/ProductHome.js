import React, {useState, useEffect} from "react";
import Skeleton from 'react-loading-skeleton';
import NavLink from '../Components/NavLink';
import Oferta from '../../images/Oferta.png';

function ProductHome(props) {
    const [loading, setLoading] = useState(true);
    return (
        <>
            <div className={`producto__content ${!loading ? "producto__content__hidden" : "producto__content__visible"}`}>
                <Skeleton className="skeleton__image" duration={1} />
                <p className="titulo__producto"><Skeleton className="skeleton__text"/></p>
                <p className="categoria__producto"><Skeleton className="skeleton__text"/></p>
            </div>
            <div className={`producto__content ${loading ? "producto__content__hidden" : "producto__content__visible"}`}>
                <NavLink
                href={route("producto",props.producto.producto_slug)}
                className="img__link"
                >
                {props.producto.producto_oferta ==true ?
                    <img src={Oferta} className="producto__oferta" alt="producto geodeco" />
                : ''}
                <img src={props.producto.url_img} className="producto__list" alt="producto geodeco" onLoad={() => setLoading(false)} />
                </NavLink>
                <p className="titulo__producto">{props.producto.producto_name}</p>
                <p className="categoria__producto">{props.producto.categoria_name}</p>
            </div>
        </>
    );
}

export default ProductHome
