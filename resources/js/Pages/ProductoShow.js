import React, { useEffect, useState, useCallback } from "react";
import ProductoSliderItem from "./ProductoSliderItem";
import { Carousel as Slides } from "react-responsive-carousel";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

function ProductoShow(props) {
    const producto = props.producto[0];
    const imagenes = props.imagenes;
    const porcentaje = 100-producto.discount;
    const consultar = () =>{
        const msj = "Hola, quisiera consultar por el producto '"+producto.name+"'";
        const consulta = document.getElementById('comment');
        const nombre = document.getElementById('name');
        consulta.innerText = msj;
        window.scrollTo({
            top:5000,
            behavior: 'smooth',
            duration: 600
        });
        setTimeout(() => {
            nombre.focus();
        }, 1000);
    }
    const [photoIndex, setPhotoIndex] = useState(0);
    const [isOpen, setIsOpen] = useState(false);
    const openModal = (i) => {
        setPhotoIndex((i) % imagenes.length)
        setIsOpen(true);
    }

    return (
        <>
        <div className="contenedor__producto__show">
                <Slides
                    className="carousel-producto"
                    showArrows={true}
                    emulateTouch={true}
                    showThumbs={false}
                    showStatus={false}
                    infiniteLoop={false}
                    swipeable={true}
                    useKeyboardArrows={true}
                    margin={0}
                    padding={0}
                    autoPlay={false}
                    transitionTime={800}
                >
                {imagenes.map((element, i) =>
                    <div key={i} onClick={() => openModal(i)}>
                        <ProductoSliderItem
                            llave={i}
                            src={element}

                        />
                    </div>
                )}
                </Slides>
                {isOpen && (
                    <Lightbox
                    mainSrc={imagenes[photoIndex]}
                    nextSrc={imagenes[(photoIndex + 1) % imagenes.length]}
                    prevSrc={imagenes[(photoIndex + imagenes.length - 1) % imagenes.length]}
                    onCloseRequest={() => setIsOpen(false)}
                    onMovePrevRequest={() =>
                        setPhotoIndex((photoIndex + imagenes.length - 1) % imagenes.length)
                    }
                    onMoveNextRequest={() =>
                        setPhotoIndex((photoIndex + 1) % imagenes.length)
                    }
                    />
                )}
            <div className="catacteristicas__producto">
                <div className="content__caract__product">
                    <h1 className="title__product">{producto.name}</h1>
                    <h1 className="description__product">{producto.description}</h1>
                    <hr className="line__product"></hr>
                    <div className="contenedor__info__producto">
                        <p className="motivo__caract__product">Precio:</p>
                        <p className="text__caract__product"><span className={`${producto.discount>0 ? "en_descuento" : ""}`}> ${number_format_price(producto.price,0, ",", ".")}</span> {producto.discount>0 ? <span className="valor__con__descuento"> ${number_format_price((porcentaje*producto.price/100) ,0, ",", ".")} <span className="badge__descuento">{producto.discount}% OFF</span></span>  : ""}</p>
                    </div>
                    <hr className="line__product"></hr>
                    <div className="contenedor__info__producto">
                        <p className="motivo__caract__product">Alto:</p>
                        <p className="text__caract__product"> {set_type_medida(producto.height)}</p>
                    </div>
                    <hr className="line__product"></hr>
                    <div className="contenedor__info__producto">
                        <p className="motivo__caract__product">Ancho:</p>
                        <p className="text__caract__product"> {set_type_medida(producto.width)}</p>
                    </div>
                    <hr className="line__product"></hr>
                    <div className="contenedor__info__producto">
                        <p className="motivo__caract__product">Profundidad:</p>
                        <p className="text__caract__product"> {set_type_medida(producto.depth)}</p>
                    </div>
                    <hr className="line__product"></hr>
                    <button className="consultar__btn" onClick={consultar}>CONSULTAR</button>
                </div>
            </div>
        </div>
        </>
    )
}

function number_format_price (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function set_type_medida(valor){
    if(valor<100) {
        valor = valor.toString().replace('.',',');
        return valor+" cm";
    }else{
        return ((valor*0.01).toFixed(1)).replace('.',',')+" mts";
    }
}
export default ProductoShow
