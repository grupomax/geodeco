import React, { useState } from "react";
import PT from "prop-types";
import { Link as Linke } from "react-router-dom";
import {
    LightgalleryProvider,
    LightgalleryItem,
    useLightgallery
} from "react-lightgallery";
import "lightgallery.js/dist/css/lightgallery.min.css";


const PhotoItem = ({ image, thumb, group }) => (
    <div style={{ maxWidth: "250px", width: "200px", padding: "5px", display:'none'}}>
    <LightgalleryItem group={group} src={image} thumb={thumb}>
        <img alt="modelo" src={image} style={{ width: "100%" }} />
    </LightgalleryItem>
    </div>
);
PhotoItem.propTypes = {
    image: PT.string.isRequired,
    thumb: PT.string,
    group: PT.string.isRequired
};

const OpenButtonWithHook = props => {
    const { openGallery } = useLightgallery();
    return (
    <p
        onClick={() => openGallery("MODELO1")}
        className={"cards__item__link"}
    >Ver más +
    </p>
    );
};
OpenButtonWithHook.propTypes = {
    className: PT.string
};


function CardItem(props) {
    var cubierto_1p =100000;
    var cubierto_2p =100000;
    var semi = 24500;
    var precio = 0;
    if(props.plantas === "1"){
        precio = (props.metros_semicubierto * semi) +( props.metros_cubierto * cubierto_1p);
    }
    else{
        precio = (props.metros_semicubierto * semi) + (props.metros_cubierto * cubierto_2p);
    }
    var total = parseFloat(props.metros_cubierto) + parseFloat(props.metros_semicubierto);

    var entrega = (precio * props.entrega) / '100';
    var resto_cuotas = precio - entrega;
    var precio_cuota = resto_cuotas / props.cuotas;
    const [visible, setVisible] = useState(true);
    var url = props.path;

    const ImgElement = props => {
        const { openGallery } = useLightgallery();
        return (
            <a href={url}>
                <img alt='Travel' className="cards__item__img" src={props.src}/>
            </a>
        );
    };
    ImgElement.propTypes = {
        className: PT.string
    };

    return (
        <>
    <div className="content" style={{display:'none'}}>
        <button
            className="button is-light"
            style={{
            position: "absolute"
            }}
            onClick={() => setVisible(!visible)}
        >
            {visible ? (
            <i className="fas fa-eye-slash" />
            ) : (
            <i className="fas fa-eye" />
            )}
        </button>
        </div>
            <li className="cards__item">
                <div>
                    <figure className="cards__item__pic-wrap">
                    <LightgalleryProvider>
                                <div
                                style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center"
                                }}
                                >
                                {props.contenedor.map((p, idx) => (
                                    <PhotoItem key={idx} image={p} group={"MODELO1"} />
                                ))}
                                </div>
                                <ImgElement src={props.src} />
                            </LightgalleryProvider>
                            <div className="cards__item__caracter">
                                <div className="cards__item__data">
                                    <p className="cards__item__data__text">{props.camas} Dormitorios</p>
                                </div>
                                <div className="cards__item__data">
                                    <p className="cards__item__data__text">{props.duchas} Baños</p>
                                </div>
                                <div className="cards__item__data">
                                    <p className="cards__item__data__text">{parseInt(total)} m<sup>2</sup></p>
                                </div>
                            </div>
                    </figure>
                    <hr className="line__gold__model"></hr>
                    <div className="cards__item__info">
                        <div className="cards__item__prince">
                            {/* <h5 className="cards__item__text">${number_format(precio,0, ",", ".")}</h5> */}
                            <div className="cards__item__info__prince">
                                <h5 className="cards__item__text">{props.label}</h5>
                                {/* <h5 className="cards__item__cuotas">+{props.cuotas} CUOTAS <strong>${ number_format(precio_cuota,0, ",", ".")}</strong></h5> */}
                                {/* <h5 className="cards__item__cuotas">ENTREGA DE <strong>${number_format(entrega,0, ",", ".")}</strong><br></br>+ cuotas</h5> */}
                                <h5 className="cards__item__cuotas">ENTREGA DE <strong>${number_format(entrega,0, ",", ".")}</strong></h5>
                                </div>
                            {visible ? (
                            <LightgalleryProvider>
                                <div
                                style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center"
                                }}
                                >
                                {props.contenedor.map((p, idx) => (
                                    <PhotoItem key={idx} image={p} group={"MODELO1"} />
                                ))}
                                </div>
                            </LightgalleryProvider>
                            ) : null}
                        </div>
                    </div>
                </div>
            </li>
        </>
    )
}

function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

export default CardItem
