import React from 'react';
import NavLink from './NavLink';
import BtnToggleActive from './BtnToggleActive';

function TrCategory({categoria}){
    return (
        <tr >
                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{categoria.name}</td>
                <td className="px-6 py-4 text-left">
                    <BtnToggleActive estado={categoria.deleted_at === null} categoria={categoria.id} />
                </td>
                <td><NavLink href={`/categorias/editar/${categoria.id}`}>Editar</NavLink></td>
        </tr>
    );
}

export default TrCategory;
