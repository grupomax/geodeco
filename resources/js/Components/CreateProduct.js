import React, {useState, useEffect, useRef} from 'react';
import Label from './Label';
import Checkbox from './Checkbox';
import classNames from 'classnames'
import CurrencyInput from 'react-currency-masked-input';
import NavLink from './NavLink';
import Button from './Button';
import slugify from 'slugify';
import Swal from 'sweetalert2';
import ProductImgItem from './ProductImgItem';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import AddPhoto from './AddPhoto';
import { Inertia } from '@inertiajs/inertia';

library.add(faTrash);
function CreateProduct({
    type = 'text',
    className,
    producto,
    categorias
}){
    const [inputName, setInputName] = useState(producto?producto.name:'');
    const [slug, setSlug] = useState(producto?producto.slug:'');
    const [currentImg, setCurrentImg] = useState(producto?producto.current_img:'');
    const [category, setCategory] = useState(producto?producto.category_id:categorias[0].id);
    const [inputPrice, setInputPrice] = useState(producto?producto.price:'');
    const [inputOffer, setInputOffer] = useState(producto?producto.offer == 0?false:true:false);
    const [discount, setDiscount] = useState(producto?producto.discount:'');
    const [seeDiscount, setSeeDiscount] = useState(producto?producto.offer == 0?false:true:false);
    const [inputWidth, setInputWidth] = useState(producto?parseInt(producto.width):0);
    const [inputHeight, setInputHeight] = useState(producto?parseInt(producto.height):0);
    const [inputDepth, setInputDepth] = useState(producto?parseInt(producto.depth):0);
    const [description, setDescription] = useState(producto?producto.description:'');
    const [processing, setProcessing] = useState(false);
    const [permitido, setPermitido] = useState('');
    const firstRenderInput = useRef(true);
    const firstRenderSlug = useRef(true);
    const firstCurrentImg = useRef(true);
    const discountInputClass = classNames({
        "w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm": true,
        "text-gray-400 bg-gray-200":!seeDiscount,
    });
    const handleName = (e) => {
        setInputName(e.target.value);
        if(e.target.value.length <= 2){
            setProcessing(true);
            setPermitido('¡Categoria demasiado corta!');
        }
    };
    function syncWithServer(slugName){
        let url = '';
        let datos = {};
        if(producto){
            url = '/productos/canslug';
            datos.slug = slugName;
            datos.prod_id = producto.id;
        } else {
            url = '/productos/canslugoncreate';
            datos.slug = slugName;
        }
        axios.post(url,datos)
            .then(({data}) => {
                if(data.status){
                    if(inputName.length <= 2){
                        setProcessing(true);
                        setPermitido('Nombre de Producto demasiado corto!');
                    } else {
                        setPermitido('¡OK!');
                        setProcessing(false);
                    }
                } else {
                    setPermitido('¡Producto Existente!');
                    setProcessing(true);
                }
            })
            .catch((e) => console.log(e));
    }
    const handleCategory = (e) => {
        setCategory(e.target.value);
    };
    const handleChange = (e,i) => {
        setInputPrice(i);
    };
    const handleCheck = () => {
        setInputOffer(!inputOffer);
        setSeeDiscount(!seeDiscount);
    };
    const handleDiscount = (e) => {
        setDiscount(e.target.value);
    };
    const handleWidth = (e) => {
        setInputWidth(e.target.value);
    }
    const handleHeight = (e) => {
        setInputHeight(e.target.value);
    };
    const handleDepth = (e) => {
        setInputDepth(e.target.value);
    };
    const handleDescription = (e) => {
        setDescription(e.target.value);
    }
    const handleCurrentImg = (e) => {
        setCurrentImg(e.currentTarget.value);
    };
    const handleTrashImg = (url, key) => {
        if(url){
            const splitted_url = url.split('/');
            Swal.fire({
                title: '¿Estás seguro?',
                text: "Se eliminará la imagen por completo",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!',
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !Swal.isLoading(),
                backdrop: true,
                preConfirm: () => {
                    axios.post('/productos/img/delete',{
                        //url: splitted_url[splitted_url.length-2] + '/' + splitted_url[splitted_url.length-1]
                        producto_id:producto.id,
                        file:splitted_url[splitted_url.length-1]
                    })
                    .then((resp) => Inertia.get(route('productos.edit',producto.id)))
                    .catch((e) => console.log(e));
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    //Swal.showLoading()
                    Swal.fire(
                        '¡Eliminado!',
                        'La imagen ha sido eliminada',
                        'success'
                    )
                }
            });
        }
    }
    const handleEliminar = () => {
        setProcessing(true);
        axios.post('/productos/destroy',{
            producto_id: producto.id
        })
        .then(() => {
            Swal.fire({
                icon:'success',
                title:'Producto Eliminado',
                showConfirmButton: true
            });
            Inertia.get(route('productos'));
        })
        .catch((e) => console.log(e));
    };
    const handleClick = (e) => {
        let url = '';
        if(producto){
            url = '/productos/update';
        } else {
            url = '/productos/store';
        }
        if(!inputName && !inputPrice){
            return Swal.fire({
                position: 'bottom-end',
                icon: 'error',
                title: '¡Error!',
                text: 'Completa toda la información',
                showConfirmButton: false,
                timer: 1500
            });
        }
            setProcessing(true);
            axios.post(url,{
                id: producto?producto.id:'',
                name: inputName,
                slug: slug,
                category_id: category,
                description: description,
                price: inputPrice,
                width: inputWidth,
                height: inputHeight,
                depth: inputDepth,
                offer: inputOffer?1:0,
                discount: discount,

            })
            .then(({data}) => {
                setProcessing(false);
                if(producto){
                    Swal.fire({
                        icon: 'success',
                        title: '¡Listo!',
                        showConfirmButton: true,
                    }).then((result) => {
                        if(result.isConfirmed){
                            Inertia.get(route('productos'));
                        }
                    });
                } else {
                    Swal.fire({
                        icon: 'success',
                        title: '¡Producto Creado!',
                        text: 'Ahora deberás subir fotos del producto, de lo contrario no se podrá ver en página.',
                        showConfirmButton: true
                    }).then((result) => {
                        if(result.isConfirmed){
                            Inertia.get(route('productos.edit',data.prod.id));
                        }
                    });
                }
            })
            .catch((e) => {
                setProcessing(false);
                console.log(e);
                Swal.fire({
                    position: 'bottom-end',
                    icon: 'error',
                    title: '¡Error!',
                    text: `No pudimos ${producto?'Actualizar':'Crear'} el Producto`,
                    showConfirmButton: false,
                    timer: 1500
                }).then(() => Inertia.get(route('productos')));
            });
    }
    useEffect(() => {
        if(!inputOffer)setDiscount(0);
    }, [inputOffer]);
    useEffect(() => {
        if (firstRenderSlug.current) {
            firstRenderSlug.current = false;
            return;
        }
        syncWithServer(slug);
    }, [slug]);
    useEffect(() => {
        if (firstRenderInput.current) {
            firstRenderInput.current = false;
            return;
        }
        setSlug(slugify(inputName));
    }, [inputName]);
    useEffect(() => {
        if (firstCurrentImg.current){
            firstCurrentImg.current = false;
            return
        }
        axios.post('/productos/update/current-img',{
            producto_id: producto.id,
            current_img: currentImg,
        })
        .then((resp) => console.log('listo'))
        .catch((e) => console.log(e));
    }, [currentImg]);
    return (
        <div className="grid grid-cols-3 gap-x-2 gap-y-10 pt-4 m-4">
            <div className="col-span-3">
                <Label for="name" value="Nombre" className="mb-2" />
                <input
                    type={type}
                    className={
                        `w-2/3 border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm ` +
                        className
                    }
                    value={inputName}
                    onChange={(e) => handleName(e)}
                />
                <span className="my-6 sm:mx-4">{permitido}</span>
            </div>
            <div>
                <Label for="categoria" value="Categoria" className="mb-2" />
                <select
                    name="categoria"
                    defaultValue={category}
                    className="w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                    onChange={(e) => handleCategory(e)}
                >
                        {categorias.map((el,index) => {
                            return <option key={index} value={el.id}>{el.name}</option>;
                        })}
                </select>
            </div>
            <div>
                <Label for="price" value="Precio de Lista" className="mb-2" />
                {/* <input
                    type={type}
                    className={
                        `w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm ` +
                        className
                    }
                    value={inputPrice}
                    onChange={(e) => handleChange(e)}
                /> */}
                <CurrencyInput
                    name="price"
                    className={
                        `w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm ` +
                        className
                    }
                    defaultValue={inputPrice?inputPrice.toString():''}
                    onChange={(e,i) => handleChange(e,i)}
                    required />
            </div>
            <div className="grid grid-cols-2 ">
                <div>
                    <Label for="offer" value="¿En Oferta?" className="mb-2" />
                        <div className="w-full h-10">
                            <Checkbox name="En Oferta" checked={inputOffer} value={inputOffer} handleChange={handleCheck} className="w-5 h-5 ml-2"/>
                            <span className="text-xl font-semibold ml-3">Sí</span>
                        </div>

                </div>
                <div>
                    <Label for="discount" value="% de Descuento" className="mb-2" />
                    <input
                        type="number"
                        min="0"
                        max="100"
                        name="discount"
                        disabled={!seeDiscount}
                        className={discountInputClass}
                        value={discount}
                        onChange={(e) => handleDiscount(e)}
                    />
                </div>
            </div>
            <div>
                <Label for="width" value="Ancho (cm.)" />
                <input
                    type="number"
                    name="width"
                    className="w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                    value={inputWidth}
                    onChange={(e) => handleWidth(e)}
                />
            </div>
            <div>
                <Label for="height" value="Alto (cm.)"/>
                <input
                    type="number"
                    name="height"
                    className="w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                    value={inputHeight}
                    onChange={(e) => handleHeight(e)}
                />
            </div>
            <div>
                <Label for="height" value="Profundidad (cm.)"/>
                <input
                    type="number"
                    name="depth"
                    className="w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                    value={inputDepth}
                    onChange={(e) => handleDepth(e)}
                />
            </div>
            <div className="col-span-3">
                <Label for="description" value="Descripción del Producto"/>
                <textarea
                    name="description"
                    cols="30"
                    rows="10"
                    className="w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                    value={description}
                    onChange={(e) => handleDescription(e)}
                ></textarea>
            </div>
            {producto &&
                <React.Fragment>
                    <div className="col-span-3">
                        <h3 className="relative text-xl font-semibold py-4">
                            Fotos
                            <div className="absolute right-0"><AddPhoto producto_id={producto.id} /></div>
                        </h3>
                        <Label for="fotos" value="Selecciona la Foto principal"/>
                        <div className="p-2 grid grid-cols-4 gap-2">
                            {
                                producto.url_imgs.map((i, key) => {
                                    return (
                                        <React.Fragment key={key}>
                                            <div>
                                                <div className="relative">
                                                    <input
                                                        type="radio"
                                                        name="currentImg"
                                                        value={i}
                                                        checked={currentImg == i}
                                                        onChange={(e) => handleCurrentImg(e)}
                                                        className="absolute left-1 top-6"
                                                    />
                                                    <button
                                                        type="button"
                                                        onClick={(e) => handleTrashImg(i, key)}
                                                        className="absolute right-1 top-6 inline-flex items-center px-2 py-2 bg-red-400 rounded-md font-semibold text-xs text-white hover:bg-red-600 transition ease-in-out duration-150">
                                                            <FontAwesomeIcon icon="trash" />
                                                    </button>
                                                </div>
                                                <ProductImgItem url={i} className="mt-4" />
                                                <p className="text-center">{currentImg == i?'¡Imagen Principal!':''}</p>
                                            </div>
                                        </React.Fragment>
                                    );
                                })
                            }
                        </div>
                    </div>
                </React.Fragment>
            }
            <div className="col-span-3">
                <div className="flex justify-between">
                    <NavLink href={route('productos')}>Volver</NavLink>
                    <Button type="button" processing={processing} onClick={(e) => handleClick(e)}>Guardar</Button>
                    {producto && <button type="button" onClick={() => handleEliminar()} className="inline-flex items-center px-4 py-2 bg-red-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-600 transition ease-in-out duration-150">Eliminar</button>}
                </div>
            </div>
        </div>
    );
}

export default CreateProduct;
