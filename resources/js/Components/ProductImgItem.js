import React, {useState} from 'react';
import Modal from 'react-modal';
import classNames from 'classnames';

//Modal.setAppElement(document.getElementById('app'));
function ProductImgItem({url, className = ''}){
    const [modalIsOpen, setIsOpen] = useState(false);
    const overlayModal = classNames({
        "fixed inset-0 backend-gradient":true,
    });
    const contentModal = classNames({
        "absolute top-2/4 left-2/4 right-auto bottom-auto mr-2/4 bg-white overflow-auto p-4 transform translate -translate-x-2/4 -translate-y-2/4":true,
    });
    const handleClick = () => {
        setIsOpen(!modalIsOpen);
    }
    return (
        <div className={`cursor-pointer ${className}`} onClick={handleClick}>
            <img src={url} />
            <Modal
                isOpen={modalIsOpen}
                shouldCloseOnOverlayClick={true}
                ariaHideApp={false}
                overlayClassName={overlayModal}
                className={contentModal}
            >
                <div className="text-center">
                    <img src={url} />
                </div>
            </Modal>
        </div>
    );
}

export default ProductImgItem;
