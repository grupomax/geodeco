import React from 'react';
import logo_claro from "../../images/iconos/geodeco-logo.png";

export default function ApplicationLogo({ className }) {
    return (
        <img
              src={logo_claro}
              className={className}
              alt=""
              />
    );
}
