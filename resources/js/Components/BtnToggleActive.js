import React, {useState} from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import Swal from 'sweetalert2';

library.add(faSyncAlt)

function BtnToggleActive({className = '', estado, categoria, producto }){
    const [realState, setRealState] = useState(estado)
    const [processing, setProcessing] = useState(false);
    function test(){
        setProcessing(true);
        if(categoria){
            axios.post('categorias/toggle-category',{
                'category_id': categoria
            })
            .then(() => {
                setProcessing(false);
                setRealState(!realState);
                Swal.fire({
                    position: 'bottom-end',
                    icon: 'success',
                    title: `Categoría ${realState?'Desactivada':'Activada'}`,
                    showConfirmButton: false,
                    timer: 1500
                })
            })
            .catch((e) => {
                setProcessing(false);
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error. intenta de nuevo más tarde',
                    showConfirmButton: false,
                    timer: 2500
                })
                console.error(e.message);
            })
        } else {
            axios.post('productos/toggle-product',{
                'product_id': producto
            })
            .then(({data}) => {
                setProcessing(false);
                if(data.status){
                    setRealState(!realState);
                    Swal.fire({
                        position: 'bottom-end',
                        icon: 'success',
                        title: `Producto ${realState?'Desactivado':'Activado'}`,
                        showConfirmButton: false,
                        timer: 1500
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: '¡Error!',
                        text: data.msg,
                        showConfirmButton: true,
                    });
                }
            })
            .catch((e) => {
                setProcessing(false);
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error. intenta de nuevo más tarde',
                    showConfirmButton: false,
                    timer: 2500
                })
                console.error(e.message);
            })
        }
    }
    return (
        <button
            onClick={test}
            className={
                `inline-flex items-center px-2 border border-transparent rounded-full text-center leading-5 font-semibold text-xs text-green-800 uppercase transition ease-in-out duration-150 hover:bg-transparent ${
                    processing && 'opacity-25'
                } ${ realState ? 'bg-green-100 hover:border hover:border-green-100' : 'bg-red-400 hover:border hover:border-red-400'} ` + className
            }
            disabled={processing}
        >
            {processing ? <FontAwesomeIcon icon="sync-alt" className="fa-spin" /> : realState ? 'Activo' : 'Inactivo'}
        </button>
    );
}

export default BtnToggleActive;
