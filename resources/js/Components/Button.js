import React from 'react';

export default function Button({ type = 'submit', className = '', processing, children, onClick = null }) {
    return (
        <button
            type={type}
            className={
                `inline-flex items-center px-4 py-2 bg-geodeco border border-transparent rounded-md font-semibold text-xs text-gray-600 uppercase tracking-widest active:bg-gray-900 transition ease-in-out duration-150 ${
                    processing && 'opacity-25'
                } ` + className
            }
            disabled={processing}
            onClick={onClick}
        >
            {children}
        </button>
    );
}
