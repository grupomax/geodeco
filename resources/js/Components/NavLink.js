import { InertiaLink } from '@inertiajs/inertia-react';
import React from 'react';

export default function NavLink({ className = '', href, active, children }) {
    return (
        <InertiaLink
            href={href}
            className={
                active
                    ? 'inline-flex items-center px-1 pt-1 border-b-2 none-outline border-indigo-400 text-sm font-semibold  leading-5 text-yellow-400 focus:outline-none focus:border-indigo-700 transition duration-150 ease-in-out '+ className 
                    : 'inline-flex items-center px-1 pt-1 border-b-2 none-outline border-transparent text-sm font-medium leading-5 text-yellow-800  hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out '
                    + className }
        >
            {children}
        </InertiaLink>
    );
}
