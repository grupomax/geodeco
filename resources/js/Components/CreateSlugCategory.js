import React, { useState, useEffect, useRef } from 'react';
import Label from '@/Components/Label';
import Button from '@/Components/Button';
import NavLink from '@/Components/NavLink';
import slugify from 'slugify';
import axios from 'axios';
import Swal from 'sweetalert2';
import { Inertia } from '@inertiajs/inertia';

function CreateSlugCategory({
    type = 'text',
    className,
    categoria
}){
    const [inputName, setInputName] = useState(categoria?categoria.name:'');
    const [slugName, setSlugName] = useState(categoria?categoria.slug:'');
    const [processing, setProcessing] = useState(true);
    const [permitido, setPermitido] = useState('');
    const firstRenderInput = useRef(true);
    const firstRenderSlug = useRef(true);


    function handleChange(e) {
        setInputName(e.target.value);
        if(e.target.value.length <= 2){
            setProcessing(true);
            setPermitido('¡Categoria demasiado corta!');
        }
    }
    function syncWithServer(slugName){
        axios.post('/categorias/canslug',{slug: slugName})
            .then(({data}) => {
                if(data.status){
                    if(inputName.length <= 2){
                        setProcessing(true);
                        setPermitido('¡Categoria demasiado corta!');
                    } else {
                        setPermitido('¡OK!');
                        setProcessing(false);
                    }
                } else {
                    setPermitido('¡Categoria Existente!');
                    setProcessing(true);
                }
            })
            .catch((e) => console.log(e));
    }
    const handleClick = (e) => {
        let url = '';
        let data = {};
        if(categoria){
            url = '/categorias/update';
            data.categoria_id = categoria.id;
        } else {
            url = '/categorias/store';
        }
        data.name = inputName;
        data.slug = slugName;
        setProcessing(true);
        axios.post(url, data)
        .then(() => {
            Swal.fire({
                position: 'bottom-end',
                icon: 'success',
                title: '¡Listo!',
                showConfirmButton: true
            })
            Inertia.get(route('categorias'));
        })
        .catch((e) => {
            console.log(e);
        });
    }
    const handleEliminar = (e) => {
        if(categoria){
            Swal.fire({
                title: '¿Seguro que quiere eliminar la categoría?',
                text:'También se eliminarán los Productos que pertenezcan a esta categoría',
                showCancelButton: true,
                confirmButtonText: 'Sí, Eliminar',
                confirmButtonColor: 'red',
                cancelButtonText:'Cancelar',
                showLoaderOnConfirm: true,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    axios.delete(`/categorias/destroy/${categoria.id}`)
                        .then(({data}) => {
                            if(data.status){
                                Swal.fire('¡Eliminado!', '', 'success');
                            } else {
                                Swal.fire('¡Error!', 'Algo sucedió y no pudimos eliminar la categoría', 'error');
                            }
                        })
                        .catch((e) => console.log(e));
                }
                if(result.isDismissed) {
                    Swal.fire({
                        position: 'bottom-end',
                        icon: 'info',
                        title: 'Acción Cancelada',
                        showConfirmButton: false,
                        timer: 1500,
                    });
                }
            })
        }
    }
    useEffect(() => {
        if (firstRenderSlug.current) {
            firstRenderSlug.current = false;
            return;
        }
        syncWithServer(slugName);
    }, [slugName]);
    useEffect(() => {
        if (firstRenderInput.current) {
            firstRenderInput.current = false;
            return;
        }
        setSlugName(slugify(inputName));
    }, [inputName]);
    return (
        <div className="grid grid-cols-1 gap-2 pt-4 m-4">
            <div>
                <Label for="name" value="Nombre" />
                <input
                    type={type}
                    className={
                        `w-full sm:w-3/4 border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm` +
                        className
                    }
                    value={inputName}
                    onChange={(e) => handleChange(e)}
                />
                <span className="my-6 sm:mx-4">{permitido}</span>
            </div>
            <div className="pt-10">
                <div className="flex justify-between">
                    <NavLink href={route('categorias')}>Volver</NavLink>
                    <Button type="button" processing={processing} onClick={(e) => handleClick(e)}>Guardar</Button>
                    {categoria && <button type="button" onClick={(e) => handleEliminar(e)} className="inline-flex items-center px-4 py-2 bg-red-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-600 transition ease-in-out duration-150">Eliminar</button>}
                </div>
            </div>
        </div>
    );
}

export default CreateSlugCategory;
