import React, {useState, useEffect, useRef} from 'react';
import Button from './Button';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faCamera } from '@fortawesome/free-solid-svg-icons';
import Modal from 'react-modal';
import classNames from 'classnames';
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";
import Label from './Label';
import axios from 'axios';
import Swal from 'sweetalert2';
import { Inertia } from '@inertiajs/inertia';

library.add(faPlus,faCamera);
function AddPhoto({producto_id}){
    const [processing, setProcessing] = useState(false);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [src,setSrc] = useState(null);
    const [image, setImage] = useState(null);
    const overlayModal = classNames({
        "fixed inset-0 backend-gradient":true,
    });
    const contentModal = classNames({
        "absolute top-2/4 left-2/4 right-auto bottom-auto mr-2/4 bg-white overflow-auto p-4 transform translate -translate-x-2/4 -translate-y-2/4 w-4/5 h-4/5":true,
    });
    const handleClick = (e) => {
        setModalIsOpen(true);
    };
    const handleCancel = () => {
        setModalIsOpen(false);
    }
    const handleFile = (e) => {
        e.preventDefault();
        console.log(e);
        let files;
        if (e.dataTransfer) {
        files = e.dataTransfer.files;
        } else if (e.target) {
        files = e.target.files;
        }
        const reader = new FileReader();
        reader.onload = () => {
            setImage(reader.result);
        };
        reader.readAsDataURL(files[0]);
    };

    const handleSubmit = () => {
        const imageElement = cropperRef?.current;
        const cropper = imageElement?.cropper;
        // Upload cropped image to server if the browser supports `HTMLCanvasElement.toBlob`.
        // The default value for the second parameter of `toBlob` is 'image/png', change it if necessary.
        cropper.getCroppedCanvas().toBlob((blob) => {
            const formData = new FormData();

            // Pass the image file name as the third parameter if necessary.
            formData.append('croppedImage', blob, `${Date.now()}.jpg`);
            formData.append('productoId', producto_id);

            // Use `jQuery.ajax` method for example
            /* $.ajax('/path/to/upload', {
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success() {
                console.log('Upload success');
            },
            error() {
                console.log('Upload error');
            },
            }); */
            axios.post('/productos/uploadImg',formData)
            .then((resp) => {
                Swal.fire({
                    title: '¡Listo!',
                    text: 'La imagen ha sido Cargada',
                    icon: 'success'
                })
                Inertia.get(route('productos.edit',producto_id));
            })
            .catch((e) => {console.log(e)})
        }, 'image/jpeg');
    }


    const cropperRef = useRef(null);
    const onCrop = () => {
        const imageElement = cropperRef?.current;
        const cropper = imageElement?.cropper;
        setSrc(cropper.getCroppedCanvas().toDataURL());
    };
    return (
        <div>
            <Button type="button" processing={processing} onClick={(e) => handleClick(e)}>
                <FontAwesomeIcon icon="plus" className="mr-2" />
                Agregar Foto
            </Button>
            <Modal
                isOpen={modalIsOpen}
                shouldCloseOnOverlayClick={false}
                ariaHideApp={false}
                overlayClassName={overlayModal}
                className={contentModal}
            >
                <div className="text-center border-b border-gray-200">
                    <h3>Subir nueva Foto</h3>
                </div>
                <div className="text-center p-4 mb-4">
                    <label htmlFor="file-upload" className="border inline-block py-2 px-4 shadow-md rounded-md cursor-pointer">
                        <FontAwesomeIcon icon="camera" className="mr-2" /> Selecciona imagen
                    </label>
                    <input id="file-upload" type="file" name="nuevafoto" accept="image/*" onChange={(e) => handleFile(e)} className="hidden"/>
                </div>

                <div className="grid grid-cols-1 sm:grid-cols-2 gap-8">
                    <div>
                    <Label value="Preview:" />
                    <Cropper
                        src={image}
                        style={{ height: 500, width: "100%" }}
                        // Cropper.js options
                        aspectRatio={1 / 1}
                        guides={false}
                        crop={onCrop}
                        ref={cropperRef}
                        />
                    </div>
                    <div>
                        <Label value="Resultado:" />
                        <img src={src} />
                    </div>
                </div>
                <div className="w-full text-center mt-4">
                    <div className="flex justify-between">
                        <Button type="button" processing={processing} onClick={handleCancel}>
                            Cancelar
                        </Button>
                        {(src &&
                            <Button type="button" processing={processing} onClick={handleSubmit}>
                                Aceptar
                            </Button>
                        )}
                    </div>
                </div>
            </Modal>
        </div>
    );
}

export default AddPhoto;
