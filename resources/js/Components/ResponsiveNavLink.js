import { InertiaLink } from '@inertiajs/inertia-react';
import React from 'react';

export default function ResponsiveNavLink({ method = 'get', as = 'a', href, active = false, children }) {
    return (
        <InertiaLink
            method={method}
            as={as}
            href={href}
            className={`w-full flex items-start pl-3 pr-4 py-2 border-l-4 ${
                active
                    ? 'border-yellow-400 bg-transparent font-semibold focus:outline-none'
                    : 'border-transparent text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300'
            } text-yellow-400 text-base font-normal focus:outline-none transition duration-150 ease-in-out`}
        >
            {children}
        </InertiaLink>
    );
}
