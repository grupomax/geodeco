import React, {useState} from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import NavLink from './NavLink';
import BtnToggleActive from './BtnToggleActive';
import { Transition } from '@headlessui/react';
import classNames from 'classnames';
import NumberFormat from 'react-number-format';
import ProductImgItem from './ProductImgItem';

library.add(faChevronDown);
function ProductoItem({producto}){
    const [toggleProd, setToggleProd] = useState(false);
    const handleToggle = () => {
        setToggleProd(!toggleProd);
    };
    const formatearDimension = (num) => {
        if(num > 100){
            return ((num / 100).toFixed(2)) + " mts.";
        }
        return num + " cm.";
    };
    const formatearPrecio = (precio, desc) => {
        if (desc){
            const condescuento = (((100 - desc) * precio) / 100);
            return (
                <React.Fragment>
                    <div className="grid grid-cols-2 gap-2">
                        <NumberFormat value={precio} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} className="line-through" />
                        <span className="bg-gray-400 px-1 py-1 text-xs text-white text-center rounded-xl shadow">{desc}% OFF</span>
                        <NumberFormat value={condescuento} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} decimalScale={2} className="col-span-2 font-semibold" />
                    </div>
                </React.Fragment>
            );
        } else {
            return <NumberFormat value={precio} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={'$'} />;
        }
    };
    const formatearDescription = (descript) => {
        if(descript){
            const parseado = descript.replaceAll('. ', '. \n');
            return (
                <React.Fragment>
                    <div>
                        {parseado.split("\n").map((i,key) => {
                            return <div key={key}>{i}</div>;
                        })}
                    </div>
                </React.Fragment>
            );
        } else {
            return "";
        }

    };
    const rowClass = classNames({
        "col-span-6 grid grid-cols-6 border-b whitespace-nowrap text-sm font-mediu, text-gray-900 transition ease-out duration-300": true,
        "transform bg-white": !toggleProd,
        "transform py-4 bg-gray-50 shadow": toggleProd,
    });
    const fontChevronClass = classNames({
        "absolute right-0 mr-5 cursor-pointer transition ease-out duration-300": true,
        "transform rotate-180": toggleProd,
        "transform rotate-0": !toggleProd,
    })
    return (
        <div className={rowClass}>
            <div className="col-span-2 px-2 truncate sm:px-6 py-4 font-semibold">{producto.name}</div>
            <div className="hidden sm:flex px-6 py-4">{producto.categorias.name}</div>
            <div className="hidden sm:flex px-6 py-4">{formatearPrecio(producto.price, producto.discount)}</div>
            <div className="col-span-2 sm:col-span-1 px-6 py-4">
                <BtnToggleActive estado={producto.on_stock} producto={producto.id} />
            </div>
            <div className="col-span-2 sm:col-span-1 relative px-2 sm:px-6 py-4">
                <NavLink href={route('productos.edit',producto.id)}>Editar</NavLink>
                <FontAwesomeIcon icon="chevron-down" className={fontChevronClass} onClick={handleToggle} />
            </div>

            <Transition
                show={toggleProd}
                enter="transition ease-out duration-300"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-200"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
                className="col-span-6"
            >
                <div className="w-full px-2 sm:px-6">
                    <div className="grid grid-cols-1 sm:grid-cols-2 gap-2">
                        <div>
                            <div className="grid grid-cols-1">
                                <div className="grid grid-cols-2 sm:grid-cols-3 p-2">
                                    <div className="col-span-2 sm:hidden">Precio: {formatearPrecio(producto.price, producto.discount)}</div>
                                    <div className="sm:hidden">Categoría: {producto.categorias.name}</div>
                                    <div>Alto: {formatearDimension(producto.height)}</div>
                                    <div>Ancho: {formatearDimension(producto.width)}</div>
                                    <div>Profundidad: {formatearDimension(producto.depth)}</div>
                                </div>
                                <div className="grid grid-cols-1">
                                    <hr />
                                    <div className="p-2">
                                        <h4 className="font-bold text-lg">Descripción:</h4>
                                        {formatearDescription(producto.description)}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="p-2 grid grid-cols-4 gap-2">
                            {
                                producto.url_imgs.map((i, key) => {
                                    return <ProductImgItem key={key} url={i} className="transition delay-150 duration-300 ease-in-out transform hover:scale-150 hover:z-10" />;
                                })
                            }
                        </div>
                    </div>
                </div>
            </Transition>
        </div>
    );
}

export default ProductoItem;
