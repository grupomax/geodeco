<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Geodeco Mail</title>
    <style>
        .card {
            transition: 0.3s;
            border: 2px black inset;
            width: auto;
        }
        .container {
            padding: 2px 16px;
        }
        .comentario {
            text-decoration: underline;
        }
        .text_comentario{
            font-size: 1.17em;
        }
    </style>
</head>
<body>
    <div class="card">
        <div class="container">
            <h3>De: {{$nombre}}</h3>
            <p class="text_comentario">Email: <b>{{$email}}<b></p>
            <p class="text_comentario">Teléfono: <b>{{$telefono}}<b></p>
            <p class="text_comentario">El cliente prefiere ser contactado entre las <b>{{$from}} hs</b> y las <b>{{$to}} hs</b></p>
            <h3 class="comentario">Comentario</h3><p class="text_comentario">"{!!$comentario!!}"</p>
        </div>
    </div>
</body>
</html>
