<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="scrollbar-thin scrollbar-thumb-geodeco scrollbar-track-transparent overflow-y-scroll scrollbar-thumb-rounded-full scrollbar-track-rounded-full">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="keywords" content="Geodeco, Muebles, Muebles de diseño, Deco, Decoración, Interiores, Diseño, Personalizados, Restauración, Córdoba">
        <meta name="author" content="Geodeco">
        <meta name="description" content="🪑 Restauración de muebles antiguos y trabajos personalizados. ✅ Ingresá ahora y conocé nuestros trabajos."/>
        <meta name="og:url" content="geodeco.com.ar">
        <meta name="og:title" content="Geodeco">
        <meta name="og:type" content="website">
        <meta name="og:site_name" content="Geodeco">
        <meta property="og:title" content="Geodeco">
        <meta name="og:description" content="🪑 Restauración de muebles antiguos y trabajos personalizados. ✅ Ingresá ahora y conocé nuestros trabajos.">
        <meta name="og:street-address" content="">
        <meta name="og:locality" content="Capital, Córdoba">
        <meta name="og:country-name" content="AR">
        <meta name="og:postal-code" content="5000">
        <meta name="og:locale" content="es_AR">
        <meta name="image" content="favicon.png">
        <meta name="og:phone_number" content="+5493513433653">
        <meta property="og:locale" content="es_AR">
        <meta http-equiv="cache-control" content="public, no-transform" />
        <META http-equiv="Expires" content="30d">
        <meta name="theme-color" content="#75DED7">

        <!-- Google / Search Engine Tags -->
        <meta itemprop="name" content="Geodeco">
        <meta itemprop="description" content="🪑 Restauración de muebles antiguos y trabajos personalizados. ✅ Ingresá ahora y conocé nuestros trabajos.">
        <meta itemprop="image" content="favicon.png">
        <meta itemprop="image:type" content="image/png" />

        <!-- Facebook Meta Tags -->
        <meta property="og:url" content="geodeco.com.ar">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Geodeco">
        <meta property="og:description" content="🪑 Restauración de muebles antiguos y trabajos personalizados. ✅ Ingresá ahora y conocé nuestros trabajos.">
        <meta property="og:image" content="favicon.ico" />
        <meta property="og:image:secure_url" content="favicon.png" />
        <meta property="og:image:type" content="image/png" />
        <!-- Twitter Meta Tags -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="Geodeco">
        <meta name="twitter:description" content="🪑 Restauración de muebles antiguos y trabajos personalizados. ✅ Ingresá ahora y conocé nuestros trabajos.">
        <meta name="twitter:image" content="favicon.png">
        <meta name="twitter:image:type" content="image/png" />

        <title>{{ config('app.name', 'Geodeco') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Abel&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- Scripts -->
        @routes
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
        <img src="favicon.png" alt="logo" style="opacity: 0; position: absolute;z-index:-1;">
        @inertia

        @env ('local')
            <script src="http://localhost:3000/browser-sync/browser-sync-client.js"></script>
        @endenv
    </body>
</html>
